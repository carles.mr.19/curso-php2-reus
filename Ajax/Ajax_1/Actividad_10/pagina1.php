<?php
    if ($_REQUEST['pa']=='esp') {
        $superficie = 505944;
        $capital = "Madrid";
        $idioma = "Castellano";
        $poblacion = 47007367;
    }

    if ($_REQUEST['pa']=='fra') {
        $superficie = 675417;
        $capital = "Paris";
        $idioma = "Francia";
        $poblacion = 67795000;
    }

    if ($_REQUEST['pa']=='ita'){
        $superficie = 301340;
        $capital = "Roma";
        $idioma = "Italiano";
        $poblacion = 60541000;
    }

    $xml="<?xml version=\"1.0\"?>\n";
    $xml.="<pais>\n";
    $xml.="<superficie>$superficie</superficie>\n";
    $xml.="<capital>$capital</capital>\n";
    $xml.="<idioma>$idioma</idioma>\n";
    $xml.="<poblacion>$poblacion</poblacion>\n";
    $xml.="</pais>\n";
    
    header('Content-Type: text/xml');
    
    echo $xml;

?>