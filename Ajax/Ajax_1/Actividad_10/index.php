<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <script src="funciones.js"></script>
    </head>

    <body>
        Selecciona un pais:
        <select id="pais">
            <option value="0" selected>seleccione</option>
            <option value="esp">España</option>
            <option value="fra">Francia</option>
            <option value="ita">Italia</option>
        </select>
        <br>
        <div id="resultados"></div>
    </body>
</html>
