<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/almacenes', function () {
    return view('almacenes');
});

Route::get('/cajas', function () {
    return view('cajas');
});

Route::get('/almacenes/crear', 'AlmacenesController@create');
Route::get('/almacenes/listar', 'AlmacenesController@index');
Route::post('/almacenes/crear', 'AlmacenesController@store');
Route::delete('/almacenes/delete/{id}', 'AlmacenesController@destroy');
Route::get('/almacenes/buscar', 'AlmacenesController@buscar');
Route::post('/almacenes/listar', 'AlmacenesController@show');
Route::post('/almacenes/editar/{id}', 'AlmacenesController@edit');
Route::post('/almacenes/update/{id}', 'AlmacenesController@update');
Route::get('/almacenes/deleteAll', 'AlmacenesController@destroyAll');

Route::get('/cajas/crear', 'CajasController@create');
Route::get('/cajas/listar', 'CajasController@index');
Route::post('/cajas/crear', 'CajasController@store');
Route::delete('/cajas/delete/{id}', 'CajasController@destroy');
Route::get('/cajas/buscar', 'CajasController@buscar');
Route::post('/cajas/listar', 'CajasController@show');
Route::post('/cajas/editar/{id}', 'CajasController@edit');
Route::post('/cajas/update/{id}', 'CajasController@update');
Route::get('/cajas/deleteAll', 'CajasController@destroyAll');