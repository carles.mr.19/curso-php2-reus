<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
<h1>Almacenes</h1>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/almacenes/crear">Añadir Registro</a></li>
    <li><a href="/almacenes/listar">Listar</a></li>
    <li><a href="/almacenes/listar">Borrar</a></li>
    <li><a href="/almacenes/buscar">Buscar</a></li>
    <li><a href="/almacenes/listar">Modificar</a></li>
    <li><a href="/almacenes/deleteAll">Borrar Todo</a></li>
</ul>

<p><a href="/">Volver al Menu</a></p>
</body>
</html>
