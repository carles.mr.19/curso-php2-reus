<html>
@extends('partials.head')
    <body>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Contenido</th>
                <th scope="col">Valor</th>
                <th scope="col">ID Almacen</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>

            <tbody>

                @foreach($cajas as $caja)
                    <tr>
                        <td>{{$caja->id}}</td>
                        <td>{{$caja->contenido}}</td>
                        <td>{{$caja->valor}}</td>
                        <td>{{$caja->id_almacen}}</td>
                        <td>
                            <form action="/cajas/delete/{{$caja->id}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </form>
                            <form action="/cajas/editar/{{$caja->id}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>