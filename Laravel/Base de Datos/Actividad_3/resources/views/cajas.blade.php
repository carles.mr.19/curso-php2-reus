<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
<h1>Cajas</h1>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/cajas/crear">Añadir Registro</a></li>
    <li><a href="/cajas/listar">Listar</a></li>
    <li><a href="/cajas/listar">Borrar</a></li>
    <li><a href="/cajas/buscar">Buscar</a></li>
    <li><a href="/cajas/listar">Modificar</a></li>
    <li><a href="/cajas/deleteAll">Borrar Todo</a></li>
</ul>

<p><a href="/">Volver al Menu</a></p>
</body>
</html>
