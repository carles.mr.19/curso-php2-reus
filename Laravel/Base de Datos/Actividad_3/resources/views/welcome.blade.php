<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/almacenes">Almacenes</a></li>
    <li><a href="/cajas">Cajas</a></li>
</ul>
</body>
</html>
