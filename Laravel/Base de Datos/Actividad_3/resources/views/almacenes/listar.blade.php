<html>
@extends('partials.head')
    <body>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Lugar</th>
                <th scope="col">Capacidad</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>

            <tbody>

                @foreach($almacenes as $almacen)
                    <tr>
                        <td>{{$almacen->id}}</td>
                        <td>{{$almacen->lugar}}</td>
                        <td>{{$almacen->capacidad}}</td>
                        <td>
                            <form action="/almacenes/delete/{{$almacen->id}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </form>
                            <form action="/almacenes/editar/{{$almacen->id}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>