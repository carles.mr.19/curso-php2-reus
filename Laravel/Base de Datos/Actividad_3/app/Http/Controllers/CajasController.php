<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class CajasController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cajas = DB::table('cajas')->get();

        return view('cajas.listar')->with('cajas', $cajas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('cajas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'contenido' => 'required',
            'valor' => 'required',
            'almacen' => 'required'
        ]);

        $insert = DB::table('cajas')->insert([
            'contenido' => $request->contenido,
            'valor' => $request->valor,
            'id_almacen' => $request->almacen
        ]);

        return redirect('/cajas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $idCaja = $request->id;

        $caja = DB::table('cajas')->where('id', '=', $idCaja)->first();

        if($caja){
            return view('cajas.show')->with('caja', $caja);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $cajas = DB::table('cajas')->where('id', $id)->first();
        return view('cajas.edit')->with('cajas', $cajas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = DB::table('cajas')->where('id', $id)->update([
            'contenido' => $request->contenido,
            'valor' => $request->valor,
            'id_almacen' => $request->almacen
        ]);

        return redirect('/cajas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('cajas')->where('id', '=', $id)->delete();
        return redirect('/cajas');
    }

    public function destroyAll() {
        DB::table('cajas')->delete();
        return redirect('/cajas');
    }

    public function buscar(){
        //$articulo = DB::table('articulos')->where('nombre', '=', $request->nombre)->get();

        return view('cajas.buscar');
    }
}
