<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class AlmacenesController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $almacenes = DB::table('almacenes')->get();

        return view('almacenes.listar')->with('almacenes', $almacenes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('almacenes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'lugar' => 'required',
            'capacidad' => 'required',
        ]);

        $insert = DB::table('almacenes')->insert([
            'lugar' => $request->lugar,
            'capacidad' => $request->capacidad,
        ]);

        return redirect('/almacenes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $idAlmacen = $request->id;

        $almacen = DB::table('almacenes')->where('id', '=', $idAlmacen)->first();

        if($almacen){
            return view('almacenes.show')->with('almacen', $almacen);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $almacenes = DB::table('almacenes')->where('id', $id)->first();
        return view('almacenes.edit')->with('almacenes', $almacenes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = DB::table('almacenes')->where('id', $id)->update([
            'lugar' => $request->lugar,
            'capacidad' => $request->capacidad
        ]);

        return redirect('/almacenes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('almacenes')->where('id', '=', $id)->delete();
        return redirect('/');
    }

    public function destroyAll() {
        DB::table('almacenes')->delete();
        return redirect('/almacenes');
    }

    public function buscar(){
        //$articulo = DB::table('articulos')->where('nombre', '=', $request->nombre)->get();

        return view('almacenes.buscar');
    }
}
