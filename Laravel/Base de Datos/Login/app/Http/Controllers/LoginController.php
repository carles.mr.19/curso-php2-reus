<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function login(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:3',
        ]);

        $email = $request->email;
        $password = md5($request->password);

        $select = \DB::table('users')->where('email','=', $email)->first();

        if($email == $select->email && $password == $select->password){
           return view('login_correcto');
        }else{
            echo "Error";
        }

    }
}
