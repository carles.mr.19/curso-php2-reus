<!DOCTYPE html>
<html>
<head>
    <title>Login Success</title>
</head>
<body>

@if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <form action="/login" method="POST">
        Email: <input type="text" name="email"><br>
        Password: <input type="text" name="password"><br>
        <input type="submit" value="Login">
    </form>
</body>
</html>
