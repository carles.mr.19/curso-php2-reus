<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Persona 1',
            'email' => 'persona@persona1.com',
            'password' => md5('password'),
        ]);
    }
}
