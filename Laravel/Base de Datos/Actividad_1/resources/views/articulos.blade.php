<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
<h1>Articulos</h1>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/articulos/crear">Añadir Registro</a></li>
    <li><a href="/articulos/listar">Listar</a></li>
    <li><a href="/articulos/listar">Borrar</a></li>
    <li><a href="/articulos/buscar">Buscar</a></li>
    <li><a href="/articulos/listar">Modificar</a></li>
    <li><a href="/articulos/deleteAll">Borrar Todo</a></li>
</ul>

<p><a href="/">Volver al Menu</a></p>
</body>
</html>
