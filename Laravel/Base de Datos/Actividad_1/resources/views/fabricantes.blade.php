<!doctype html>
<html lang="es">
    @extends('partials.head')
    <body>
    <h1>Fabricantes</h1>
    @if ($errors->has('name'))
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
        <ul>
            <li><a href="/fabricantes/crear">Añadir Registro</a></li>
            <li><a href="/fabricantes/listar">Listar</a></li>
            <li><a href="/fabricantes/listar">Borrar</a></li>
            <li><a href="/fabricantes/buscar">Buscar</a></li>
            <li><a href="/fabricantes/listar">Modificar</a></li>
            <li><a href="/fabricantes/deleteAll">Borrar Todo</a></li>
        </ul>

    <p><a href="/">Volver al Menu</a></p>
    </body>
</html>
