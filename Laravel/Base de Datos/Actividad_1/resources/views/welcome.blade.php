<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/fabricantes">Fabricantes</a></li>
    <li><a href="/articulos">Articulos</a></li>
</ul>
</body>
</html>
