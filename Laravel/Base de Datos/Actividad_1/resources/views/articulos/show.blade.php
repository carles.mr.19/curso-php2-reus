<html>
@extends('partials.head')
<body>
<table class="table">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Nombre</th>
        <th scope="col">Precio</th>
        <th scope="col">ID_Fabricante</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$articulo->id}}</td>
            <td>{{$articulo->nombre}}</td>
            <td>{{$articulo->precio}}</td>
            <td>{{$articulo->id_fabricante}}</td>
        </tr>
    </tbody>
</table>
</body>
</html>