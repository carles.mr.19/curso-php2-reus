<html>
@extends('partials.head')
    <body>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Precio</th>
                <th scope="col">ID_Fabricante</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>

            <tbody>

                @foreach($articulos as $articulo)
                    <tr>
                        <td>{{$articulo->id}}</td>
                        <td>{{$articulo->nombre}}</td>
                        <td>{{$articulo->precio}}</td>
                        <td>{{$articulo->id_fabricante}}</td>
                        <td>
                            <form action="/articulos/delete/{{$articulo->id}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </form>
                            <form action="/articulos/editar/{{$articulo->id}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>