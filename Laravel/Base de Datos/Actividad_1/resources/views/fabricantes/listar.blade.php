<html>
@extends('partials.head')
    <body>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
            </tr>
            </thead>

            <tbody>

                @foreach($fabricantes as $fabricante)
                    <tr>
                        <td>{{$fabricante->id}}</td>
                        <td>{{$fabricante->nombre}}</td>
                        <td>
                            <form action="/fabricantes/delete/{{$fabricante->id}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </form>
                            <form action="/fabricantes/editar/{{$fabricante->id}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>