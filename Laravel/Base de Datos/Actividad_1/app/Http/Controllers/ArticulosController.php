<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class ArticulosController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $articulos = DB::table('articulos')->get();

        return view('articulos.listar')->with('articulos', $articulos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('articulos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required',
            'fabricante' => 'required'
        ]);

        $insert = DB::table('articulos')->insert([
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'id_fabricante' => $request->fabricante
        ]);

        return redirect('/articulos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $idArticulo = $request->id;

        $articulo = DB::table('articulos')->where('id', '=', $idArticulo)->first();

        if($articulo){
            return view('articulos.show')->with('articulo', $articulo);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $articulo = DB::table('articulos')->where('id', $id)->first();
        return view('articulos.edit')->with('articulo', $articulo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = DB::table('articulos')->where('id', $id)->update([
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'id_fabricante' => $request->fabricante
        ]);

        return redirect('/articulos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('articulos')->where('id', '=', $id)->delete();
        return redirect('/articulos');
    }

    public function destroyAll() {
        DB::table('articulos')->delete();
        return redirect('/articulos');
    }

    public function buscar(){
        return view('articulos.buscar');
    }
}
