<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class FabricaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $fabricantes = DB::table('fabricantes')->get();

        return view('fabricantes.listar')->with('fabricantes', $fabricantes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('fabricantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'nombre' => 'required',
        ]);

        $insert = DB::table('fabricantes')->insert([
            'nombre' => $request->nombre,
        ]);

        return redirect('/fabricantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $idFabricante = $request->id;

        $fabricante = DB::table('fabricantes')->where('id', '=', $idFabricante)->first();

        if($fabricante){
            return view('fabricantes.show')->with('fabricante', $fabricante);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $fabricante = DB::table('fabricantes')->where('id', $id)->first();
        return view('fabricantes.edit')->with('fabricante', $fabricante);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = DB::table('fabricantes')->where('id', $id)->update([
            'nombre' => $request->nombre,
        ]);

        return redirect('/fabricantes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('fabricantes')->where('id', '=', $id)->delete();
        return redirect('/fabricantes');
    }

    public function destroyAll() {
        DB::table('fabricantes')->delete();
        return redirect('/fabricantes');
    }

    public function buscar(){
        return view('fabricantes.buscar');
    }
}
