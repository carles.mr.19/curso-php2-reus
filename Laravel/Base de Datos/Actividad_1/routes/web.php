<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/articulos', function () {
    return view('articulos');
});

Route::get('/fabricantes', function () {
    return view('fabricantes');
});

Route::get('/articulos/crear', 'ArticulosController@create');
Route::get('/articulos/listar', 'ArticulosController@index');
Route::post('/articulos/crear', 'ArticulosController@store');
Route::delete('/articulos/delete/{id}', 'ArticulosController@destroy');
Route::get('/articulos/buscar', 'ArticulosController@buscar');
Route::post('/articulos/listar', 'ArticulosController@show');
Route::post('/articulos/editar/{id}', 'ArticulosController@edit');
Route::post('/articulos/update/{id}', 'ArticulosController@update');
Route::get('/articulos/deleteAll', 'ArticulosController@destroyAll');

Route::get('/fabricantes/crear', 'FabricaController@create');
Route::get('/fabricantes/listar', 'FabricaController@index');
Route::post('/fabricantes/crear', 'FabricaController@store');
Route::delete('/fabricantes/delete/{id}', 'FabricaController@destroy');
Route::get('/fabricantes/buscar', 'FabricaController@buscar');
Route::post('/fabricantes/listar', 'FabricaController@show');
Route::post('/fabricantes/editar/{id}', 'FabricaController@edit');
Route::post('/fabricantes/update/{id}', 'FabricaController@update');
Route::get('/fabricantes/deleteAll', 'FabricaController@destroyAll');