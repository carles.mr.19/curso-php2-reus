<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/empleados', function () {
    return view('empleados');
});

Route::get('/departamentos', function () {
    return view('departamentos');
});

Route::get('/empleados/crear', 'EmpleadosController@create');
Route::get('/empleados/listar', 'EmpleadosController@index');
Route::post('/empleados/crear', 'EmpleadosController@store');
Route::delete('/empleados/delete/{id}', 'EmpleadosController@destroy');
Route::get('/empleados/buscar', 'EmpleadosController@buscar');
Route::post('/empleados/listar', 'EmpleadosController@show');
Route::post('/empleados/editar/{id}', 'EmpleadosController@edit');
Route::post('/empleados/update/{id}', 'EmpleadosController@update');
Route::get('/empleados/deleteAll', 'EmpleadosController@destroyAll');

Route::get('/departamentos/crear', 'DepartamentosController@create');
Route::get('/departamentos/listar', 'DepartamentosController@index');
Route::post('/departamentos/crear', 'DepartamentosController@store');
Route::delete('/departamentos/delete/{id}', 'DepartamentosController@destroy');
Route::get('/departamentos/buscar', 'DepartamentosController@buscar');
Route::post('/departamentos/listar', 'DepartamentosController@show');
Route::post('/departamentos/editar/{id}', 'DepartamentosController@edit');
Route::post('/departamentos/update/{id}', 'DepartamentosController@update');
Route::get('/departamentos/deleteAll', 'DepartamentosController@destroyAll');