<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class DepartamentosController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $departamentos = DB::table('departamentos')->get();

        return view('departamentos.listar')->with('departamentos', $departamentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('departamentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'nombre' => 'required',
            'presupuesto' => 'required',
        ]);

        $insert = DB::table('departamentos')->insert([
            'nombre' => $request->nombre,
            'presupuesto' => $request->presupuesto,
        ]);

        return redirect('/departamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $idDepartamento = $request->id;

        $departamento = DB::table('departamentos')->where('id', '=', $idDepartamento)->first();

        if($departamento){
            return view('departamentos.show')->with('departamento', $departamento);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $departamento = DB::table('departamentos')->where('id', $id)->first();
        return view('departamentos.edit')->with('departamento', $departamento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = DB::table('departamentos')->where('id', $id)->update([
            'nombre' => $request->nombre,
            'presupuesto' => $request->presupuesto
        ]);

        return redirect('/departamentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('departamentos')->where('id', '=', $id)->delete();
        return redirect('/');
    }

    public function destroyAll() {
        DB::table('departamentos')->delete();
        return redirect('/departamentos');
    }

    public function buscar(){
        //$articulo = DB::table('articulos')->where('nombre', '=', $request->nombre)->get();

        return view('departamentos.buscar');
    }
}
