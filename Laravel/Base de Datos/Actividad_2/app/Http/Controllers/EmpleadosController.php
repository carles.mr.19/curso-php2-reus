<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \DB;

class EmpleadosController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empleados = DB::table('empleados')->get();

        return view('empleados.listar')->with('empleados', $empleados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('empleados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'dni' => 'required',
            'nombre' => 'required',
            'apellidos' => 'required',
            'departamento' => 'required'
        ]);

        $insert = DB::table('empleados')->insert([
            'dni' => $request->dni,
            'nombre' => $request->nombre,
            'apellidos' => $request->apellidos,
            'id_departamento' => $request->departamento,
        ]);

        return redirect('/empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $dniEmpleado = $request->dni;

        $empleado = DB::table('empleados')->where('dni', '=', $dniEmpleado)->first();

        if($empleado){
            return view('empleados.show')->with('empleado', $empleado);
        }else{
            echo "No existe el DNI.";
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($dni) {
        $empleado = DB::table('empleados')->where('dni', $dni)->first();
        return view('empleados.edit')->with('empleado', $empleado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dni)
    {
        $updates = DB::table('empleados')->where('dni', $dni)->update([
            'dni' => $request->dni,
            'nombre' => $request->nombre,
            'apellidos' => $request->apellidos,
            'id_departamento' => $request->departamento
        ]);

        return redirect('/empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($dni) {
        DB::table('empleados')->where('dni', '=', $dni)->delete();
        return redirect('/');
    }

    public function destroyAll() {
        DB::table('empleados')->delete();
        return redirect('/empleados');
    }

    public function buscar(){
        //$articulo = DB::table('articulos')->where('nombre', '=', $request->nombre)->get();

        return view('empleados.buscar');
    }
}
