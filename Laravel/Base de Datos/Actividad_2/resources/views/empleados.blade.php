<!doctype html>
<html lang="es">
    @extends('partials.head')
    <body>
    <h1>Empleados</h1>
    @if ($errors->has('name'))
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
        <ul>
            <li><a href="/empleados/crear">Añadir Registro</a></li>
            <li><a href="/empleados/listar">Listar</a></li>
            <li><a href="/empleados/listar">Borrar</a></li>
            <li><a href="/empleados/buscar">Buscar</a></li>
            <li><a href="/empleados/listar">Modificar</a></li>
            <li><a href="/empleados/deleteAll">Borrar Todo</a></li>
        </ul>

    <p><a href="/">Volver al Menu</a></p>
    </body>
</html>
