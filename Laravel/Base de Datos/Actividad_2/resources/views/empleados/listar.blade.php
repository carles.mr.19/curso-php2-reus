<html>
@extends('partials.head')
    <body>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Precio</th>
                <th scope="col">ID_Fabricante</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>

            <tbody>

                @foreach($empleados as $empleado)
                    <tr>
                        <td>{{$empleado->dni}}</td>
                        <td>{{$empleado->nombre}}</td>
                        <td>{{$empleado->apellidos}}</td>
                        <td>{{$empleado->id_departamento}}</td>
                        <td>
                            <form action="/empleados/delete/{{$empleado->dni}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </form>
                            <form action="/empleados/editar/{{$empleado->dni}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>