<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
<h1>Departamentos</h1>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/departamentos/crear">Añadir Registro</a></li>
    <li><a href="/departamentos/listar">Listar</a></li>
    <li><a href="/departamentos/listar">Borrar</a></li>
    <li><a href="/departamentos/buscar">Buscar</a></li>
    <li><a href="/departamentos/listar">Modificar</a></li>
    <li><a href="/departamentos/deleteAll">Borrar Todo</a></li>
</ul>

<p><a href="/">Volver al Menu</a></p>
</body>
</html>
