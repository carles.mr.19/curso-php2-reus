<html>
@extends('partials.head')
    <body>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Presupuesto</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>

            <tbody>

                @foreach($departamentos as $departamento)
                    <tr>
                        <td>{{$departamento->id}}</td>
                        <td>{{$departamento->nombre}}</td>
                        <td>{{$departamento->presupuesto}}</td>
                        <td>
                            <form action="/departamentos/delete/{{$departamento->id}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </form>
                            <form action="/departamentos/editar/{{$departamento->id}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>