<html>
@extends('partials.head')
<body>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Presupuesto</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$departamento->nombre}}</td>
            <td>{{$departamento->presupuesto}}</td>
        </tr>
    </tbody>
</table>
</body>
</html>