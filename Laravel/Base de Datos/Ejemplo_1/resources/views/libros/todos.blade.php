<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        @foreach($libros as $libro)
        <p>
            {{$libro['book']}}, escrito por {{$libro['author']}}
            <br>
            ISBN: {{$libro['isbn']}}
        </p>
        @endforeach
    </body>
</html>
