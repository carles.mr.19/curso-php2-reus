<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookController extends Controller
{
    public function index() {
        $libros = Book::all();
        return view('libros.todos', ['libros' => $libros->toArray()]);
    }
    
    public function show($id){
        $libro = Book::find($id);
        if(!is_null($libro)){
            return view('libros.mostrar',['libro' => $libro->toArray()]);
        }else{
            return response('No encontrado', 404);
        }
    }

    public function create(){
        return view('libros.formlibro');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|min:5',
            'author' => 'required|min:1',
            'isbn' => 'required'
        ]);

        Book::create([
            'book' => $request->name,
            'isbn' => $request->isbn,
            'author' => $request->author
        ]);

       return redirect('/libros');
    }

}
