<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/peliculas', function () {
    return view('peliculas');
});

Route::get('/salas', function () {
    return view('salas');
});

Route::get('/peliculas/crear', 'PeliculasController@create');
Route::get('/peliculas/listar', 'PeliculasController@index');
Route::post('/peliculas/crear', 'PeliculasController@store');
Route::delete('/peliculas/delete/{id}', 'PeliculasController@destroy');
Route::get('/peliculas/buscar', 'PeliculasController@buscar');
Route::post('/peliculas/listar', 'PeliculasController@show');
Route::post('/peliculas/editar/{id}', 'PeliculasController@edit');
Route::post('/peliculas/update/{id}', 'PeliculasController@update');
Route::get('/peliculas/deleteAll', 'PeliculasController@destroyAll');

Route::get('/salas/crear', 'SalasController@create');
Route::get('/salas/listar', 'SalasController@index');
Route::post('/salas/crear', 'SalasController@store');
Route::delete('/salas/delete/{id}', 'SalasController@destroy');
Route::get('/salas/buscar', 'SalasController@buscar');
Route::post('/salas/listar', 'SalasController@show');
Route::post('/salas/editar/{id}', 'SalasController@edit');
Route::post('/salas/update/{id}', 'SalasController@update');
Route::get('/salas/deleteAll', 'SalasController@destroyAll');