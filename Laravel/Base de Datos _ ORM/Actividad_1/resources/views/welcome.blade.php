<!doctype html>
<html lang="es">
@extends('partials.head')
<body>
@if ($errors->has('name'))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
<ul>
    <li><a href="/peliculas">Peliculas</a></li>
    <li><a href="/salas">Salas</a></li>
</ul>
</body>
</html>
