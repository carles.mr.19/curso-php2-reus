<html>
@extends('partials.head')
    <body>
    <h2>Salas</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">ID_Pelicula</th>
            <th scope="col">Accion</th>
        </tr>
        </thead>

        <tbody>

        @foreach($salas as $sala)
            <tr>
                <td>{{$sala->id}}</td>
                <td>{{$sala->nombre}}</td>
                <td>{{$sala->id_pelicula}}</td>
                <td>
                    <form action="/salas/delete/{{$sala->id}}" method="POST" class="float-left">
                        @csrf @method('DELETE')
                        <button type="submit" class="btn-danger btn-sm">Elimminar</button>
                    </form>
                    <form action="/salas/editar/{{$sala->id}}" method="POST" class="float-left">
                        @csrf
                        <button type="submit" class="btn-primary btn-sm">Editar</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    </body>
</html>