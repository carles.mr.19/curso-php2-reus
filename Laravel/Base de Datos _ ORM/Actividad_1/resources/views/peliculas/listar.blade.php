<html>
@extends('partials.head')
    <body>
    <h2>Peliculas</h2>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Calificacion Edad</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>

            <tbody>

                @foreach($peliculas as $pelicula)
                    <tr>
                        <td>{{$pelicula->id}}</td>
                        <td>{{$pelicula->nombre}}</td>
                        <td>{{$pelicula->calificacion_edad}}</td>
                        <td>
                            <form action="/peliculas/delete/{{$pelicula->id}}" method="POST" class="float-left">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn-danger btn-sm">Elimminar</button>
                            </form>
                            <form action="/peliculas/editar/{{$pelicula->id}}" method="POST" class="float-left">
                                @csrf
                                <button type="submit" class="btn-primary btn-sm">Editar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </body>
</html>