<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    public function sala() {
        return $this->hasMany('App\Sala');
    }
}
