<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    public function pelicula(){
        $this->belongsTo('App\Pelicula');
    }
}
