<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Sala;

class PeliculasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $peliculas = Pelicula::all();

        return view('peliculas.listar')->with('peliculas', $peliculas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('peliculas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $pelicula = new Pelicula;

        $pelicula->nombre = $request->nombre;
        $pelicula->calificacion_edad = $request->calificacion;

        $pelicula->save();

        return view('peliculas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $pelicula = Pelicula::find($request->id);

        return view('peliculas.show')->with('pelicula', $pelicula);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $pelicula = Pelicula::find($id);

        return view('peliculas.edit')->with('pelicula', $pelicula);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $pelicula = Pelicula::find($id);

        $pelicula->nombre = $request->nombre;
        $pelicula->calificacion_edad = $request->calificacion;

        $pelicula->save();

        return view('peliculas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $pelicula = Pelicula::find($id);
        $pelicula->delete();

        return view('peliculas');
    }

    public function destroyAll(){

    }

    public function buscar(){
        return view('peliculas.buscar');
    }
}
