<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sala;

class SalasController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $salas = Sala::all();

        return view('salas.listar')->with('salas', $salas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('salas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $sala = new Sala;

        $sala->nombre = $request->nombre;
        $sala->id_pelicula = $request->pelicula;

        $sala->save();

        return view('salas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $sala = Sala::find($request->id);

        return view('salas.show')->with('sala', $sala);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $sala = Sala::find($id);

        return view('salas.edit')->with('sala', $sala);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $sala = Sala::find($id);

        $sala->nombre = $request->nombre;
        $sala->id_pelicula = $request->pelicula;

        $sala->save();

        return view('peliculas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $sala = Sala::find($id);
        $sala->delete();

        return view('salas');
    }

    public function destroyAll(){

    }

    public function buscar(){
        return view('salas.buscar');
    }
}
