<?php

namespace App\Http\Middleware;

use Closure;

class LunesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        echo date("j/n/Y");

        if(date('D') === 'Mon'){
            echo "<br>Bienvenido!<br>";
        }else{
            echo "<br>No es lunes<br>";
        }

        return $next($request);
    }
}
