<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
        
	<title>Error 404</title>

	<link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}" />

</head>

<body>
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h3>Oops! Pagina no encontrada</h3>
		<h1><span>4</span><span>0</span><span>4</span></h1>
            </div>
        	
            <h2>Lo sentimos, la pagina que busca no se ha encontrado</h2>  
        </div>
    </div>
</body>

</html>
