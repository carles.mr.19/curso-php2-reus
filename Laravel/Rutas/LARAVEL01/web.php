<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/paises', function(){
    return "GET PAISES";
});

Route::post('/paises', function(){
    return "POST PAISES";
});

Route::get('paises/{pais}', function($pais){
    return "GET PAISES /$pais";
});

Route::match(array('PUT', 'PATCH'), '/paises/{pais}', function($pais){
    return "PUT PAISES /$pais";
});

Route::delete('/paises/{pais}', function($pais){
    return "DELTE PAISES /$pais";
});

Route::get('/paises/{pais}/departamentos', function($pais){
    return "GET PAISES /$pais/departamentos";
});

Route::post('/paises/{pais}/departamentos', function($pais){
    return "POST PAISES /$pais/departamentos";
});

Route::get('/paises/{pais}/departamentos/{departamento}', function($pais, $departamento){
    return "GET PAISES /$pais/departamentos/$departamento";
});

Route::match(array('PUT', 'PATCH'), '/paises/{pais}/departamentos/{departamento}', function($pais, $departamento){
    return "PUT PAISES /$pais/departamentos/$departamento";
});

Route::delete('/paises/{pais}/departamentos/{departamento}', function($pais, $departamento){
    return "DELETE PAISES /$pais/departamentos/$departamento";
});