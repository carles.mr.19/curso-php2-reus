<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(array('GET', 'POST', 'UPDATE', 'DELETE'), 'colaboradores/{nombre}', function($nombre){
    return "exmeple.com/colaboradores/$nombre";
});

Route::match(array('GET', 'POST', 'UPDATE', 'DELETE'), '/tienda/productos/{id}', function($id){
    return "exmeple.com/tienda/productos/$id";
});

Route::match(array('GET', 'POST', 'UPDATE', 'DELETE'), '/agenda/{mes}/{ano}', function($mes, $ano){
    return "exmeple.com/agenda/$mes/$ano";
});

Route::match(array('GET', 'POST', 'UPDATE', 'DELETE'), '/categoria/{lenguaje}/{id}', function($lenguaje, $id){
    return "exmeple.com/categoria/$lenguaje/$id";
});