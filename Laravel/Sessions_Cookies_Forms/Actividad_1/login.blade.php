<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        @if ($errors->any())
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form method="post" action="/login">
            <label>Usuario: </label> <input type="text" name="nombre"> <br>
            <label>Contraseña: </label> <input type="text" name="password"> <br>
            <input type="submit" value="Comprobar">
        </form>
    </body>
</html>
