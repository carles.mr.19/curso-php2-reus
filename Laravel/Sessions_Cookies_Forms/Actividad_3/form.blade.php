<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        @if ($errors->any())
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="/paises" method="GET">
            Altura: <input type="text" name="altura"><br>
            Diametro: <input type="text" name="diametro"><br>
            
            <input type="submit" value="Enviar">
        </form>
        
        
    </body>
</html>
