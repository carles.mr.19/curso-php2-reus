<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Request $request) {
        if($request->isMethod('post')){
            $altura = $request->altura;
            $diametro = $request->diametro;   
            
            $resultado = 3.14 * (($diametro / 2) ^ 2) * $altura;
            
            echo $resultado;
        }else{
            return view('form');
        }
    }
}
