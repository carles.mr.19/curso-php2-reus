<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Request $request) {
        if($request->isMethod('post')){
            $altura = $request->altura;
            $diametro = $request->diametro;
            $caudal = $request->caudal;
            
           $radio = $diametro / 2;
    
            $areaCilindro = 3.14 * $radio^2 * $altura;
            $areaBase = $areaCilindro * $altura;
    
            $capacidad = ($areaCilindro / $areaBase) * $caudal;
    
            echo "Resultado = ".$capacidad;
        }else{
            return view('form');
        }
    }
}
