<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        @if ($errors->any())
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="/paises" method="POST">
           Altura:<br> <input type="text" name="altura"><br>
            Diametro:<br><input type="text" name="diametro"><br>
            Caudal: (Litros por segundos) <br><input type="text" name="caudal"><br>
            
            <input type="submit" value="Calcular">
        </form>
        
        
    </body>
</html>
