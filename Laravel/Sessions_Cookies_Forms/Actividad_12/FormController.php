<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Request $request){

        session(['texto' => $request->texto]);

        return redirect('/comprobar');
    }

    public function comprobar(){
        $texto = session('texto');

        if($texto == strtoupper($texto)){
            session(['resultado' => "La palabra es MAYUSUCULA"]);
        }elseif($texto == strtolower($texto)){
            session(['resultado' => "La palabra es MINISCULA"]);
        }else{
            session(['resultado' => "No  se ha escrito nada"]);
        }

        return redirect('/form')->with('text', $texto);
    }
}
