<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Request $request){

        session(['texto' => $request->texto]);

        return redirect('/comprobar');
    }

    public function comprobar(){
        $texto = session('texto');

        if($texto){
            echo "Se ha escrito un texto";
        }else{
            echo "No se ha escrito nada";
        }

        return redirect('/form')->with('text', $texto);
    }
}
