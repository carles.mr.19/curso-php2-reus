<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function personalizar(){
        $tamano_fuente = \Request::cookie('color', 'black');
        
        return view('form',['color' => $tamano_fuente]);
    }
    
    public function guardarPersonalizacion(Request $request){
        $this-> validate($request, ['color' => 'required']);
        
        return redirect('/personalizacion')->withCookie(cookie('color', $request -> input('color'), 60 * 24 * 365 ));
    }
}
