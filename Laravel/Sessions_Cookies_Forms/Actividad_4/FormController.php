<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Request $request) {
        if($request->isMethod('post')){
            $tienda1 = $request->tienda1;
            $tienda2 = $request->tienda2;
            $tienda3 = $request->tienda3;
            
            $resultado = $tienda1 + $tienda2 + $tienda3;
            
            echo "Resultado: ".$resultado;
        }else{
            return view('form');
        }
    }
}
