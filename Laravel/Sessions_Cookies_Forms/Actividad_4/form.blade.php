<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        @if ($errors->any())
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="/paises" method="POST">
            Tienda 1: <input type="text" name="tienda1"><br>
            Tienda 2: <input type="text" name="tienda2"><br>
            Tienda 3: <input type="text" name="tienda3"><br>
            
            <input type="submit" value="Enviar">
        </form>
        
        
    </body>
</html>
