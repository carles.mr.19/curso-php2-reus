<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;

class FormController extends Controller {
       
    public function index(){    
        Cookie::queue('cookieTemporal', 'valor', 15);
        return view('form');
    }
    
    public function comprobar(Request $request){
        $comprobarCookie = \Request::cookie('cookieTemporal');
        
        if($comprobarCookie){
            return "El navegador soporta Cookies";
        }else{
            return "El navegador NO soporta Cookies";
        }
    }
   
}
