<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Request $request) {
        
        $paisesEspaña = array('Barcelona', 'Girona', 'Tarragona');
        $paisesFrancia = array('Paris', 'Orleans', 'Montpellier');
        
        if($request->isMethod('post')){
            if($request->pais == "España"){
                $info = $paisesEspaña;
            }else if($request->pais == "Francia"){
                $info = $paisesFrancia;
            }
            return view('tabla',  compact('info'));           
        }else{
            $info = $paisesEspaña;
            return view('tabla', compact('info'));
        }        
    }
}
