<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        @if ($errors->any())
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="/paises" method="POST">
             <select name="pais">
                <option>España</option>
                <option>Francia</option>
            </select> 
            
            <input type="submit" value="Enviar">
        </form>
        
        @foreach($info as $i)
        <p> {{$i}}</p>
        @endforeach
    </body>
</html>
