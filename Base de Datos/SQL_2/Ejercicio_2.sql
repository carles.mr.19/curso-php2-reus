--1--
SELECT apellidos FROM empleados;

--2--
SELECT DISTINCT apellidos FROM empleados;

--3--
SELECT * FROM empleados WHERE apellidos = "Lopez";

--4--
SELECT * FROM empleados WHERE apellidos = "Lopez" OR apellidos="Perez";

--5--
SELECT * FROM empleados WHERE departamento = 14;

--6--
SELECT * FROM empleados WHERE departamento = 14 OR departamento = 17;

--7--
SELECT * FROM empleados WHERE apellidos LIKE "P%";

--8--
SELECT SUM(presupuesto) FROM departamentos;

--9--
SELECT departamento , COUNT(DNI) FROM empleados GROUP BY  departamento;

--10--
SELECT * FROM empleados INNER JOIN  departamentos ON empleados.departamento = departamentos.codigo;

--11--
SELECT e.nombre, e.apellidos, d.nombre, d.presupuesto FROM empleados AS  e, departamentos AS d WHERE e.departamento = d.codigo;

--12--
SELECT e.nombre, e.apellidos FROM empleados AS  e, departamentos AS d WHERE e.departamento = d.codigo AND d.presupuesto > 60000;

--13--
SELECT * FROM departamentos WHERE presupuesto > (SELECT AVG(presupuesto) FROM departamentos);

--14--
SELECT * FROM departamentos WHERE presupuesto > (SELECT AVG(presupuesto) FROM departamentos);

--15--
INSERT INTO departamentos VALUES(100,  "Calidad", 40000);
INSERT INTO empleados VALUES(89267109,  "Esther","Vazquez", 11);

--16--
UPDATE departamentos SET presupuesto = presupuesto * 0.9;

--17--
UPDATE empleados SET departamento = 14 WHERE departamento = 77;

--18--
DELETE FROM empleados WHERE departamento = 14;

--19--
DELETE FROM empleados WHERE departamento = (SELECT codigo FROM departamentos WHERE presupuesto >= 60000);

--20--
DELETE FROM empleados;
