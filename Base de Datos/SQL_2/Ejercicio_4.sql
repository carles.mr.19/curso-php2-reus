--1--
SELECT  nombre FROM peliculas;

--2--
SELECT DISTINCT calificacionedad FROM peliculas;

--3--
SELECT codigo , nombre FROM peliculas WHERE calificacionedad IS NULL;

--4--
SELECT * FROM salas WHERE pelicula IS NULL;

--5--
SELECT * FROM salas LEFT JOIN peliculas ON salas.pelicula = peliculas.codigo;

--6--
SELECT * FROM peliculas LEFT JOIN salas on peliculas.codigo = salas.pelicula;

--7--
SELECT peliculas.nombre FROM peliculas LEFT JOIN salas on peliculas.codigo = salas.pelicula WHERE salas.pelicula IS NULL;

--8--
INSERT INTO peliculas VALUES (10, 'Uno, Dos, Tres', 7);

--9--
UPDATE peliculas SET calificacionedad = 13 WHERE calificacionedad IS NULL; 

--10--
DELETE FROM salas WHERE pelicula IN (SELECT codigo FROM peliculas WHERE calificacionedad ='G');