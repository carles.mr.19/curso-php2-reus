--1--
SELECT * FROM almacenes;

--2--
SELECT  * FROM cajas WHERE valor >150;

--3--
SELECT  DISTINCT  contenido FROM  cajas ;

--4--
SELECT  AVG(valor) FROM cajas ;

--5--
SELECT  almacen , AVG(valor) FROM  cajas GROUP BY  almacen;

--6--
SELECT almacen , AVG(valor) FROM cajas GROUP BY  almacen HAVING AVG (valor) >150;

--7--
SELECT numreferencia ,lugar FROM almacenes INNER JOIN cajas ON almacenes.codigo = cajas.almacen;

--8--
SELECT almacen , COUNT(*) FROM cajas GROUP BY almacen;

--9--
SELECT codigo
FROM almacenes 
WHERE capacidad > (
    SELECT COUNT(*) FROM cajas WHERE almacen = codigo
);

--10--
SELECT numreferencia
FROM cajas
WHERE almacen IN (
	SELECT codigo FROM almacenes WHERE lugar='Bilbao'
);

--11--
INSERT INTO almacenes VALUES (6, 'Barcelona' , 3);

--12--
INSERT INTO cajas VALUES  ('H5RT' , 'Papel' , 200, 2 );

--13--
UPDATE cajas SET VALOR = VALOR * 0.85;

--14--
UPDATE cajas SET VALOR = VALOR * 0.80 WHERE VALOR  > (SELECT AVG(Valor)FROM cajas);

--15--
DELETE FROM cajas WHERE VALOR < 100;

--16--
DELETE FROM cajas WHERE almacen = (
	SELECT codigo FROM almacenes WHERE capacidad < (
		SELECT COUNT(*) FROM cajas WHERE almacen = codigo
    )
);