--1--
SELECT nombre FROM actividades.articulos;

--2--
SELECT nombre, precio FROM actividades.articulos;

--3--
SELECT nombre FROM actividades.articulos WHERE precio <= 200;

--4--
SELECT nombre FROM actividades.articulos WHERE precio BETWEEN 60 AND 120;

--5--
SELECT nombre, precio * 166.386 AS precioPesetas FROM actividades.articulos;

--6--
SELECT AVG(precio) FROM actividades.articulos;

--7--
SELECT AVG(precio) FROM actividades.articulos WHERE fabricante = 2;

--8--
SELECT COUNT(codigo) FROM actividades.articulos WHERE precio >= 180;

--9--
SELECT nombre, precio FROM actividades.articulos WHERE precio >= 180 ORDER BY precio DESC, nombre ASC ;

--10--
SELECT * FROM actividades.articulos INNER JOIN  actividades.fabricantes ON fabricantes.codigo = articulos.fabricante;

--11--
SELECT articulos.nombre, articulos.precio, fabricantes.nombre FROM actividades.articulos INNER JOIN  actividades.fabricantes ON fabricantes.codigo = articulos.fabricante;

--12--
SELECT fabricante, AVG(precio) FROM actividades.articulos GROUP BY fabricante;

--13--
SELECT AVG(articulos.precio), fabricantes.nombre FROM actividades.articulos INNER JOIN  actividades.fabricantes ON fabricantes.codigo = articulos.fabricante GROUP BY fabricantes.nombre;

--14--
SELECT f.nombre FROM actividades.articulos 
AS a,  actividades.fabricantes AS f
 WHERE a.CODIGO = f.CODIGO 
 GROUP BY f.nombre 
 HAVING AVG(a.precio) <= 150;

--15--
SELECT nombre, precio FROM actividades.articulos ORDER BY precio ASC LIMIT 1;

--16--
SELECT fabricantes.nombre, articulos.nombre, MAX(articulos.precio) FROM actividades.articulos INNER JOIN  actividades.fabricantes ON fabricantes.codigo = articulos.fabricante GROUP BY fabricantes.nombre;

--17--
INSERT INTO articulos VALUES(11, Altavoces, 70, 2);

--18--
UPDATE actividades.articulos SET nombre = "Impresora Laser" WHERE codigo = 8

--19--
UPDATE actividades.articulos SET precio = precio * 0.9;

--20--
UPDATE actividades.articulos SET precio = precio - 10 WHERE precio >= 120;