CREATE DATABASE instituto;

USE instituto;

CREATE TABLE alumnos(
   dni VARCHAR(9) PRIMARY KEY, 
   nombre VARCHAR(50),
   apellido1 VARCHAR(50),
   apellido2 VARCHAR(50),
   direccion VARCHAR(100),
   sexo VARCHAR(15),
   fechaNacimineto DATE,
   curso INT,

   FOREIGN KEY curso 
   REFERENCES cursos(codCurso)
   ON DELETE CASCADE 
   ON UPDATE CASCADE
);

CREATE TABLE cursos(
    codCurso INT PRIMARY KEY,
    nombreCurso VARCHAR(50),
    dniProfesor VARCHAR(9),
    maxAlumnos INT,
    fechaInicio DATE, 
    fechaFin DATE,
    numHoras INT,

    FOREIGN KEY dniProfesor
    REFERENCES profesores(dni)
    ON DELETE CASCADE 
    ON UPDATE CASCADE

);

CREATE TABLE profesores(
   dni VARCHAR(9) PRIMARY KEY, 
   nombre VARCHAR(50),
   apellido1 VARCHAR(50),
   apellido2 VARCHAR(50),
   direccion VARCHAR(100),
   titulo VARCHAR(50),
   gana INT
);

