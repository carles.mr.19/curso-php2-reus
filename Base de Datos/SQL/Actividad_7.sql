CREATE DATABASE director;

USE director;

CREATE TABLE despachos(
    numero INT PRIMARY KEY,
    capacidad INT(5)
);

CREATE TABLE directores (
    dni VARCHAR(9) PRIMARY KEY,
    nombre VARCHAR(50),
    Dni_Jefe VARCHAR(9),
    despacho INT,

    FOREIGN KEY despacho
    REFERENCES despachos(numero)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY Dni_Jefe
    REFERENCES directores(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

);