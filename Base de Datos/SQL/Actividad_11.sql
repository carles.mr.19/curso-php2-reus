CREATE DATABASE investigador;

USE investigador;

CREATE TABLE facultad(
    codigo INT PRIMARY KEY,
    nombre VARCHAR(100)
);

CREATE TABLE investigadores(
    dni VARCHAR(9) PRIMARY KEY,
    nomApels VARCHAR(255),
    facultad INT,

    FOREIGN KEY facultad
    REFERENCES facultad(codigo)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);

CREATE TABLE reserva(
    dni VARCHAR(9),
    numSerie VARCHAR(4),
    comienzo DATETIME,
    fin DATETIME,
    PRIMARY KEY(dni, numSerie),

    FOREIGN KEY dni
    REFERENCES investigadores(dni)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    FOREIGN KEY numSerie
    REFERENCES equipos(numSerie)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);

CREATE TABLE equipos(
    numSerie VARCHAR(4) PRIMARY KEY,
    nombre VARCHAR(100),
    facultad INT,

    FOREIGN KEY facultad
    REFERENCES facultad(codigo)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);

