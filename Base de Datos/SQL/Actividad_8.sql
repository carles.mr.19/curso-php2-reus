CREATE DATABASE piezasProveedores;

USE piezasProveedores;

CREATE TABLE piezas(
    codigoPieza INT PRIMARY KEY,
    nombre VARCHAR(50)
);

CREATE TABLE proveedores(
    id INT PRIMARY KEY,
    nombre VARCHAR(50)
);

CREATE TABLE suministra (
    codigoPieza INT 
    idProveedor VARCHAR(4),
    precio INT,

    PRIMARY KEY(codigoPieza, idProveedor)

    FOREIGN KEY (codigoPieza) 
    REFERENCES piezas(codigoPieza) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    FOREIGN KEY (idProveedor) 
    REFERENCES proveedores(id) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
);


