CREATE DATABASE empleo;

USE empleo;

CREATE TABLE empleados(
    DNI VARCHAR(9) PRIMARY KEY,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    departamento INT,

    FOREIGN KEY departamento
    REFERENCES departamento(codigo)
    ON DELETE NULL
    ON UPDATE NULL
);

CREATE TABLE departamentos (
    codigo INT PRIMARY KEY,
    nombre VARCHAR(100),
    presupuesto INT(20)
);