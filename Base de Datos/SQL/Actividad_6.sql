CREATE DATABASE peliculasSalas;

USE peliculasSalas;

CREATE TABLE peliculas (
    codigoPelicula INT PRIMARY KEY,
    nombre VARCHAR(50),
    calificacionEdadd INT
);

CREATE TABLE salas(
    codigoSala INT PRIMARY KEY,
    nombre VARCHAR(50),
    pelicula INT,

    FOREIGN KEY pelicula
    REFERENCES peliculas(codigoPelicula)
    ON DELETE CASCADE
    ON UPDATE CASCADE    
);

