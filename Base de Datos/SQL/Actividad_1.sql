CREATE DATABASE estacionMuestra;

USE estacionMuestra;

CREATE TABLE estacion(
    identificador INT PRIMARY KEY,
    latitud INT,
    longitud INT,
    altitud INT
);

CREATE TABLE muestra(
    identificadorMuestra INT PRIMARY KEY,
    identificadorEstacion INT, 
    fecha DATE,
    temperaturaMaxima INT,
    temperaturaMinima INT,
    precipitaciones INT,
    humedadMinima   INT,
    humedadMaxima   INT,
    velocidadVientoMinima INT,
    velocidadVientoMaxima INT,

    FOREIGN KEY(identificadorEstacion)
    REFERENCES estacion(identificador)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);