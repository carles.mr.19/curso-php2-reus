CREATE DATABASE biblioteca;

USE biblioteca;

CREATE TABLE editorial(
    claveEditorial INT PRIMARY KEY,
    nombre VARCHAR(50),
    direccion VARCHAR(50), 
    telefono VARCHAR(20)
);

CREATE TABLE libro(
    claveLibro INT PRIMARY KEY,
    titulo VARCHAR(25),
    idioma VARCHAR(20),
    formato VARCHAR(20),
    claveEditorial INT,
    FOREIGN KEY (claveEditorial)
    REFERENCES editorial(claveEditorial)
    ON DELETE SET NULL,
    ON UPDATE CASCADE,
);

CREATE TABLE tema(
    claveTema INT PRIMARY KEY,
    nombre VARCHAR(40)
);

CREATE TABLE autor(
    claveAutor INT PRIMARY KEY,
    nombre VARCHAR(40),
)

CREATE TABLE ejemplar(
    claveEjemplar INT PRIMARY KEY,
    claveLibro INT, 
    numeroOrden INT(10),
    edicion INT(10),
    ubicacion VARCHAR(25),
    categoria VARCHAR(25),

    FOREIGN KEY (claveLibro)
    REFERENCES libro(claveLibro)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE socio(
    claveSocio INT PRIMARY KEY,
    nombre VARCHAR(50),
    direccion VARCHAR(50),
    telefono VARCHAR(20),
    categoria VARCHAR(25),
);

CREATE TABLE prestamo(
    clavePrestamo INT PRIMARY KEY,
    claveSocio INT,
    claveEjemplar INT, 
    numeroOrden INT(10),
    fechaPrestamo DATE,
    fechaDevolucion DATE,
    notas VARCHAR(50),

    FOREIGN KEY (claveSocio)
    REFERENCES socio(claveSocio)
    ON DELETE SET NULL
    ON UPDATE CASCADE,

    FOREIGN KEY (claveEjemplar)
    REFERENCES ejemplar(claveEjemplar)
    ON DELETE SET NULL
    ON UPDATE CASCADE
);

CREATE TABLE trata_sobre(
    id INT PRIMARY KEY,
    claveLibro INT,
    claveTema INT,
    
    FOREIGN KEY (claveLibro)
    REFERENCES libro(claveLibro)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    FOREIGN KEY (claveTema)
    REFERENCES tema(claveTema)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE esctiro_por(
    id INT PRIMARY KEY,
    claveLibro INT,
    claveAutor INT,

    FOREIGN KEY (claveLibro)
    REFERENCES libro(claveLibro)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY (claveAutor)
    REFERENCES autor(claveAutor)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE autor(

);
