CREATE DATABASE almacen;

USE almacen;

CREATE TABLE almacenes(
    codigo INT PRIMARY KEY,
    lugar VARCHAR(50),
    capacidad INT(20)
);

CREATE TABLE cajas (
    numReferencia VARCHAR(5) PRIMARY KEY,
    contenido VARCHAR(50),
    valor INT(5),
    almacen INT,

    FOREIGN KEY almacen
    REFERENCES almacenes(codigo)
    ON DELETE NULL
    ON UPDATE NULL
);