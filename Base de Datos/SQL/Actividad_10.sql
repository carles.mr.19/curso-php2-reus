CREATE DATABASE almacenes;

USE almacenes;

CREATE TABLE cajeros(
    codigo INT PRIMARY KEY;
    NomApels VARCHAR(255);
);

CREATE TABLE productos(
    codigo INT PRIMARY KEY,
    nombre VARCHAR(100),
    precio INT
);

CREATE TABLE maquinas_registradoras(
    codigo INT PRIMARY KEY,
    piso INT
);

CREATE TABLE venta (
    cajero INT, 
    maquina INT, 
    producto INT,

    PRIMARY KEY(cajero, maquina, producto)

    FOREIGN KEY cajero 
    REFERENCES cajeros(codiog)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    FOREIGN KEY maquina
    REFERENCES maquinas_registradoras(codigo)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    FOREIGN KEY producto
    REFERENCES productos(codigo)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);


