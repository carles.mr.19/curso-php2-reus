CREATE DATABASE cientificos;

USE cientificos;

CREATE TABLE cientificos(
    DNI VARCHAR(9) PRIMARY KEY,
    nombre VARCHAR(50)
);

CREATE TABLE proyecto(
    id INT PRIMARY KEY, 
    nombre VARCHAR(255),
    horas INT(5)
);

CREATE TABLE asignado_a (
    cientifico INT,
    proyecto VARCHAR(4)

    PRIMARY KEY(cientifico, proyecto)

    FOREIGN KEY cientifico
    REFERENCES cientificos(cientifico)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,

    FOREIGN KEY proyecto
    REFERENCES proyecto(id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE,
);
