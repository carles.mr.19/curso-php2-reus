CREATE DATABASE tiendaInformatica;

USE tiendaInformatica;

CREATE TABLE fabricantes(
    codigoFabricante INT PRIMARY KEY,
    nombre VARCHAR(50)
);

CREATE TABLE articulos (
    codigoArticulo INT PRIMARY KEY,
    codigoFabricante INT,
    nombre VARCHAR(50),
    precio INT(10),

    FOREIGN KEY (codigoFabricante)
    REFERENCES fabricantes(codigoFabricante)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);