
    function numero(key) {
        var result = getPantalla();
        
        if (result!='0' || isNaN(key)){
            escribirPantalla(result + key);
        }else{
            escribirPantalla(key);
        } 
    }

    function escribirPantalla(value) {
        document.getElementById('numeros').value = value;

    }

    function getPantalla() {
        return(document.getElementById('numeros').value);
    }

    function calc() {
        var result = eval(getPantalla()); 
        escribirPantalla(result);
       
    }
    
    function limpiar() { 
        escribirPantalla(0);
    }

    function raiz(){
        var val = getPantalla();
        var result = Math.sqrt(val);
        escribirPantalla(result);
    }

    function opuesto(){
        var val = getPantalla();
        var res = val.replace("+","-");
        escribirPantalla(res);
       
    }

    function inverso(){
        var val = getPantalla();
        var res = val.replace("*","/");
        escribirPantalla(res);
    }

    function retr(){
        var val = getPantalla();
        val = val.substring(0,val.length-1);
        escribirPantalla(val);
    }