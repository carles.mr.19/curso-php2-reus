var valores = [true, 5, false, "hola", "adios", 2];

//1.
console.log("Parte 1");
var resFinal = valores[3].length > valores[4].length;
console.log(resFinal);

//2.
//Obtener true
console.log("Parte 2");
var res = valores[0] || valores[2];
console.log(res);

//Obtener false
var res2 = valores[0] && valores[2];
console.log(res2);

//3.
console.log("Parte 3");
var suma = valores[1] + valores[5];
var resta = valores[1] - valores[5];
var mult = valores[1] * valores[5];
var div = valores[1] / valores[5];
var mod = valores[1] % valores[5];

console.log(suma);
console.log(resta);
console.log(mult);
console.log(div);
console.log(mod);
