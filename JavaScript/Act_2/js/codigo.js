//Al cargar la pagina se ejecutaran estos dos mensajes:

//Definimos un mensaje por consola, "Hola Mundo".
console.log("Hola mundo");
//Definimos un mensaje por consola, "Soy el primer script".
console.log("Soy el primer script");

//Definimos la variable mensaje.
var mensaje = "Hola Mundo! \nQué facil es incluir \'comillas simples\' y \"comillas dobles\" ";
//Ejecutamos la funcion alert mas el mensaje para mostrarlo por pantalla.
alert(mensaje);

