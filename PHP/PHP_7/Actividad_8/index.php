<html>
    <body>
        <?php
            class Celda {
                private $texto;
                private $colorFuente;
                private $colorFondo;
                
                function __construct($tex,$cfue,$cfon){
                    $this->texto=$tex;
                    $this->colorFuente=$cfue;
                    $this->colorFondo=$cfon;
                }
                
                public function graficar(){
                    echo '<td style="color:'.$this->colorFuente.';background-color:'.$this->colorFondo.'">'.$this->texto.'</td>';
                }
            }

            class Tabla {
                private $celdas = array();
                private $cantFilas;
                private $cantColumnas;

                public function __construct($fi,$co){
                    $this->cantFilas=$fi;
                    $this->cantColumnas=$co;
                }

                public function insertar($celda,$fila,$columna){
                    $this->celdas[$fila][$columna]=$celda;
                }

                private function inicioTabla(){
                    echo '<table border="1">';
                }

                private function inicioFila(){
                    echo '<tr>';
                }

                private function mostrar($filas,$columnas){
                    $this->celdas[$filas][$columnas]->graficar(); 
                }

                private function finFila(){
                    echo '</tr>';
                }

                private function finTabla(){
                    echo '</table>';
                }

                public function graficar(){
                    $this->inicioTabla();
                    for($i =1; $i<=$this->cantFilas;$i++){
                        $this->inicioFila();
                        
                        for($x=1;$x<=$this->cantColumnas;$x++){
                            $this->mostrar($i,$x);
                        }
                        
                        $this->finFila();
                    }
                    $this->finTabla();
                }
            }

        $tabla1=new Tabla(3,2);
        $celda=new Celda('Celda 1','#000000','#ffffff');
        $tabla1->insertar($celda,1,1);
        $celda=new Celda('Celda 2','#000000','#ffffff');
        $tabla1->insertar($celda,1,2);

        $celda=new Celda('Contenido Celda 1','#000000','#5bb2f5');
        $tabla1->insertar($celda,2,1);
        $celda=new Celda('Contenida Celda 2','#000000','#5bb2f5');
        $tabla1->insertar($celda,2,2);
        
        $celda=new Celda('Contenido Celda 1.2','#000000','#5bb2f5');
            $tabla1->insertar($celda,3,1);
        $celda=new Celda('Contenida Celda 2.2','#000000','#5bb2f5');
        $tabla1->insertar($celda,3,2);
        $tabla1->graficar();
    ?>
</body>
</html>