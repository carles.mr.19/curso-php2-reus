<html>
    <body>
<?php
        class Celda {
            private $texto;
            private $colorFuente;
            private $colorFondo;
            
            function __construct($tex,$cfue,$cfon){
                $this->texto=$tex;
                $this->colorFuente=$cfue;
                $this->colorFondo=$cfon;
            }
            
            public function graficar(){
                echo '<td style="color:'.$this->colorFuente.';background-color:'.$this->colorFondo.'">'.$this->texto.'</td>';
            }
        }   

        class Tabla {
            private $celdas=array();
            private $cantFilas;
            private $cantColumnas;

            public function __construct($fi,$co){
                $this->cantFilas=$fi;
                $this->cantColumnas=$co;
            }

            public function cargar($fila,$columna,$valor,$cfue,$cfon){
                $this->celdas[$fila][$columna]=new Celda($valor,$cfue,$cfon);
            }

            private function inicioTabla(){
                echo '<table border="1">';
            }

            private function inicioFila(){
                echo '<tr>';
            }

            private function mostrar($fi,$co){
                $this->celdas[$fi][$co]->graficar(); 
            }

            private function finFila(){
                echo '</tr>';
            }

            private function finTabla(){
                echo '</table>';
            }

            public function graficar(){
                $this->inicioTabla();
                for($f=1;$f<=$this->cantFilas;$f++){
                    $this->inicioFila();
                    
                    for($c=1;$c<=$this->cantColumnas;$c++){
                        $this->mostrar($f,$c);
                    }
                    
                    $this->finFila();
                }
                
                $this->finTabla();
            }
        }

        $tabla1 =  new Tabla(3,2);
        $tabla1->cargar(1,1,"Celda 1","#000000","#ffffff");
        $tabla1->cargar(1,2,"Celda 2","#000000","#ffffff");

        $tabla1->cargar(2,1,"Contenido Celda 1","#000000","#5bb2f5");
        $tabla1->cargar(2,2,"Contenida Celda 2","#000000","#5bb2f5");
        $tabla1->cargar(3,1,"Contenido Celda 1.2","#000000","#5bb2f5");
        $tabla1->cargar(3,2,"Contenida Celda 2.2","#000000","#5bb2f5");
        $tabla1->graficar();
?>
</body>
</html>