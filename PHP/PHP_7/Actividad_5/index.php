<html>
    <body>
<?php

    class Tabla {
        private $mat = array();
        private $cantFilas;
        private $cantColas;
        
        public function __construct($nFilas, $nCol){
            $this -> cantFilas = $nFilas;
            $this -> cantColas = $nCol;
        }
        
        public function cargar($fila, $columna, $valor){
            $this->mat[$fila][$columna] = $valor;
        }
        
        private function mostrar($fi, $co){
            echo '<td>'.$this->mat[$fi][$co].'</td>';
        }


        private function inicioTabla(){
            echo '<table border="1">';
        }
        
        private function inicioFila(){
            echo '<tr>';
        }
        
        private function finFila(){
            echo '</tr>';
        }
        
        private function finTabla(){
            echo '</table>';
        }
        
        public function graficar(){
            $this ->inicioTabla();
            for($i = 1; $i <= $this->cantFilas; $i++){
                $this ->inicioFila();
                for($x = 1; $x <= $this->cantColas; $x++){
                    $this->mostrar($i, $x);
                }
                $this->finFila();
                
            }
            $this->finTabla();
        }
        
    }
    
    $tabla1 = new Tabla(2,3);
    $tabla1 -> cargar(1,1,"1");
    $tabla1 -> cargar(1,2,"2");
    $tabla1 -> cargar(1,3,"3");
    $tabla1 -> cargar(2,1,"4");
    $tabla1 -> cargar(2,2,"5");
    $tabla1 -> cargar(2,3,"6");
    $tabla1 -> graficar();
    
            
        ?>
    </body>
</html>
