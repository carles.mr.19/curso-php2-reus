<?php

    class Empleado {
        
        private $nombre;
        private $sueldo;
        
        public function iniciar($nombreEmpleado, $sueldoEmpleado){
            $this -> nombre = $nombreEmpleado;
            $this -> sueldo = $sueldoEmpleado;
        }
        
        public function mostrar(){
            echo "Nombre: $this->nombre<br>";
            echo "Sueldo: $this->sueldo<br>";
            if($this->sueldo > 3000){
                echo "Tiene que pagar impuestos";
            }else{
                echo "No tiene que pagar impuestos";
            }
        }
    }
    
    $empleado = new Empleado();
    $empleado -> iniciar("Carles", 3001);
    $empleado -> mostrar();
    

?>