<?php

    class Empleado {
        
        private $nombre;
        private $sueldo;
        
        public function __construct($nombreEmpleado, $sueldoEmpleado = "1000"){
            $this -> nombre = $nombreEmpleado;
            $this -> sueldo = $sueldoEmpleado;
        }
        
        public function mostrar(){
            echo "Nombre: $this->nombre<br>";
            echo "Sueldo: $this->sueldo<br>";
        }
    }
    
    $empleado = new Empleado("Carles");
    $empleado -> mostrar();
    
    $empleado2 = new Empleado("Arnau", 3001);
    $empleado2 -> mostrar();
    

?>