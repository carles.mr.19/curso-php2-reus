<html>
    <body>
<?php

    class CabeceraPagina {
        private $titulo;
        private $ubicacion;
        private $colorFuente;
        private $colorFondo;
        
        public function __construct($tit, $ubi, $colorFuen, $colorFon){
            $this -> titulo = $tit;
            $this -> ubicacion = $ubi;
            $this -> colorFuente = $colorFuen;
            $this -> colorFondo = $colorFon;
        }
        
        public function mostrar(){
            echo '<div style="text-align:'.$this -> ubicacion.';color:'.$this->colorFuente.';background-color:'.$this->colorFondo.'">';
            echo $this->titulo;
            echo '</div>';
        }
    }
    
    $cabecera = new CabeceraPagina('Hola', 'center', '#000000','#ff001e');
    $cabecera -> mostrar();
            
        ?>
    </body>
</html>
