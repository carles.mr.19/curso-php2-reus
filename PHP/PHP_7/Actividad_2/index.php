<?php
    class Menu {
        private $enlaces=array();
        private $titulos=array();
        
        public function cargarOpcion($tit, $en){
            $this->enlaces[]=$en;
            $this->titulos[]=$tit;
        }
        
        public function mostrarHorizontal(){
            for($i=0;$i<count($this->enlaces);$i++){
                echo '<a href="'.$this->enlaces[$i].'">'.$this->titulos[$i].'</a>';
                echo "-";
            }
        }
        
        public function mostrarVertical(){
            for($i=0;$i<count($this->enlaces);$i++){
                echo '<a href="'.$this->enlaces[$i].'">'.$this->titulos[$i].'</a>';
                echo "<br>";
            }
        }
    }
 
    $menu1=new Menu();
    $menu1->cargarOpcion('Google', 'www.google.com');
    $menu1->cargarOpcion('Yhahoo', 'www.yahoo.com');
    $menu1->cargarOpcion('MSN', 'www.msn.com');
    $menu1->mostrarHorizontal();
?>
</body>
</html>