<?php
    class Persona {
        protected $nombre;
        protected $edad;
        
        public function cargarDatosPersonales($nom,$ed){
            $this->nombre=$nom;
            $this->edad=$ed;
        }
        
        public function imprimirDatosPersonales(){
            echo 'Nombre: '.$this->nombre.'<br>';
            echo 'Edad: '.$this->edad.'<br>';
        }
        
        public function setEdad($edad){
            $this->edad = $edad;
        }
    }   

    class Empleado extends Persona{
        protected $sueldo;
        
        public function cargarSueldo($su){
            $this->sueldo=$su;
        }
        
        public function imprimirSueldo(){
            echo 'Sueldo:'.$this->sueldo.'<br>';
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];
    $sueldo = $_REQUEST['sueldo'];

    $empleado = new Empleado();
    $persona = new Persona();
    $persona -> cargarDatosPersonales('Carles',20);
    $persona ->setEdad(34);
    
    $persona ->imprimirDatosPersonales();
?>