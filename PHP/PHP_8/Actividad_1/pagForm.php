<?php
    class Operacion {
        protected $valor1;
        protected $valor2;
        protected $resultado;
        
        public function cargarValor1($v){
            $this->valor1=$v;
        }
        
        public function cargarValor2($v){
            $this->valor2=$v;
        }
        
        public function imprimirResultado(){
            echo $this->resultado.'<br>';
        }
    }

    class Suma extends Operacion{
        public function operar(){
            $this->resultado = $this->valor1 + $this->valor2;
        }
    }

    class Resta extends Operacion{
        public function operar(){
            $this->resultado = $this->valor1 - $this->valor2;
        }
    }
    
    $valor1 = $_REQUEST['valor1'];
    $valor2 = $_REQUEST['valor2'];
    
    $suma = new Suma();
    $suma->cargarValor1($valor1);
    $suma->cargarValor2($valor2);
    $suma->operar();
    echo 'El resultado de la suma de '.$valor1.' + '.$valor2.' es: ';
    $suma->imprimirResultado();
    
    
    $resta=new Resta();
    $resta->cargarValor1($valor1);
    $resta->cargarValor2($valor2);
    $resta->operar();
    echo '<br>El resultado de la resta de '.$valor1.' - '.$valor2.' es: ';
    $resta->imprimirResultado();
   

?>