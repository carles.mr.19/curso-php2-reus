<?php
    class Persona {
        protected $nombre;
        protected $edad;
        
        public function cargarDatosPersonales($nom,$ed){
            $this->nombre=$nom;
            $this->edad=$ed;
        }
        
        public function imprimirDatosPersonales(){
            echo 'Nombre:'.$this->nombre.'<br>';
            echo 'Edad:'.$this->edad.'<br>';
        }
    }

    class Empleado extends Persona{
        protected $sueldo;
        
        public function cargarSueldo($su){
            $this->sueldo=$su;
        }
        
        public function imprimirSueldo(){
            echo 'Sueldo:'.$this->sueldo.'<br>';
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];
    $sueldo = $_REQUEST['sueldo'];
    
    $persona = new Persona();
    $persona ->cargarDatosPersonales($nombre, $edad);
    echo 'Datos personales de PERSONA: <br>';
    $persona ->imprimirDatosPersonales();
    
    $empleado1=New Empleado();
    $empleado1->cargarDatosPersonales($nombre, $edad);
    $empleado1->cargarSueldo($sueldo);
    echo '<br>Datos personales de EMPLEADO: <br>';
    $empleado1->imprimirDatosPersonales();
    $empleado1->imprimirSueldo();
?>