<?php
    class Operacion {
        protected $valor1;
        protected $valor2;
        protected $resultado;
        
        public function __construct($v1, $v2) {
            $this-> valor1 = $v1;
            $this-> valor2 = $v2;
        }
        public function imprimirResultado(){
            echo $this->resultado.'<br>';
        }
    }

    class Suma extends Operacion{
        
        protected $titulo;
        
        public function __construct($v1, $v2, $tit) {
            Operacion::__construct($v1, $v2);
            $this-> titulo = $tit;
        }
        
        public function operar(){
            echo $this->titulo;
            echo $this-> valor1.' + '.$this->valor2.' es: ';
            $this->resultado = $this->valor1 + $this->valor2;
        }
    }

     class Resta extends Operacion{
        
        protected $titulo;
        
        public function __construct($v1, $v2, $tit) {
            Operacion::__construct($v1, $v2);
            $this-> titulo = $tit;
        }
        
        public function operar(){
            echo $this->titulo;
            echo $this-> valor1.' - '.$this->valor2.' es: ';
            $this->resultado = $this->valor1 - $this->valor2;
        }
    }
    
    $valor1 = $_REQUEST['valor1'];
    $valor2 = $_REQUEST['valor2'];
    
    $suma = new Suma($valor1, $valor2, "Suma de valores: ");
    $suma->operar();
    $suma->imprimirResultado();
    
    $resta = new Resta($valor1, $valor2, "Resta de valores: ");
    $resta->operar();
    $resta->imprimirResultado();
   

?>