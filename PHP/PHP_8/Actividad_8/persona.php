<?php

    class Persona{
        protected $nombre;
        protected $edad;
        protected $sexo;
        protected $faltas;

        public function __construct($nombre, $edad, $sexo="indefinido") {
            $this->nombre = $nombre;
            $this->edad = $edad;
            $this->sexo = $sexo;
        }

        public function getFaltas() {
            return $this->faltas;
        }

        public function setFaltas($faltas) {
            $this->faltas = $faltas;
        }


        public function getNombre() {
            return $this->nombre;
        }

        public function getEdad() {
            return $this->edad;
        }

        public function getSexo() {
            return $this->sexo;
        }
    }
?>