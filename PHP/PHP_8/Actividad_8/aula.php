<?php

    class Aula{
        private $id;
        private $maxEstudiantes;
        private $horas;
        private $materia;
        private $profesor;
        private $alumnos = [];


        public function __construct($id, $materia, $maxEstudiantes, $horas) {
            $this->id = $id;
            $this->maxEstudiantes = $maxEstudiantes;
            $this->horas = $horas;
            $this->materia = $materia;
        }
        
        public function getId() {
            return $this->id;
        }

        public function getMaxEstudiantes() {
            return $this->maxEstudiantes;
        }

        public function getHoras() {
            return $this->horas;
        }

        public function getMateria() {
            return $this->materia;
        }

        public function getProfesor() {
            return $this->profesor;
        }


        public function setId($id) {
            $this->id = $id;
        }

        public function setMaxEstudiantes($maxEstudiantes) {
            $this->maxEstudiantes = $maxEstudiantes;
        }

        public function setHoras($horas) {
            $this->horas = $horas;
        }

        public function setMateria($materia) {
            $this->materia = $materia;
        }

        public function setProfesor($profesor) {
            $this->profesor = $profesor;
        }

        public function assignarProfe(Profesor $profesor){
            if($this->materia == $profesor->getMateria() ){
                $this->setProfesor($profesor->getNombre());
                echo "<br>El profesor de la classe ".$this->getId(). " se llama ".$this->getProfesor();
            }
        }

        public function darClase(Profesor $profesor, $alumnos){
            $faltaAlumnos = 0;
            $aprovados = 0;

            $prof = ($profesor->getFaltas() * $this->horas) / 100;

            if($prof<=20){
                $totAlumnos = sizeof($alumnos);

                foreach ($alumnos as $alumno) {
                    $falta = ($alumno->getFaltas() * $this->horas) / 100;
                    
                    if($falta<=40){
                        $faltaAlumnos+=1;
                    }

                    if($alumno->getCalificacion() >=5){
                        $aprovados+=1;
                    }
                }

                if( (($faltaAlumnos * $this->getMaxEstudiantes()) /100) <50 ){
                    echo "<br>Alumnos aprovados $aprovados";
                }

            }
        }

        public function assignarAlumnos(Estudiante $estudiante){
            if($estudiante instanceof Estudiante && sizeof($this->alumnos) <= $this->getMaxEstudiantes()){
                array_push($this->alumnos, $estudiante);
                echo "Alumno ". $estudiante->getNombre() ." de la clase ".$this->getId(). " ha sido añadido<br>";
            }
        }
        
    }
?>