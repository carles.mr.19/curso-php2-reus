<?php

    require 'persona.php';
    require 'estudiante.php';
    require 'profesor.php';
    require 'aula.php';

    $clase = new Aula(1, "matematicas", 4, 20);
    $profe = new Profesor("Gerard", 45, "H", "matematicas");
    
    $estudiantes = [
        new Estudiante("Carles", 20, "H", 10),
        new Estudiante("Xavier", 19, "H", 4),
        new Estudiante("Claudia", 21, "D", 4),
        new Estudiante("Paula", 22, "D", 7),
    ];
    
    $clase->assignarProfe($profe);
    
    echo "<br><br>";
    
    foreach ($estudiantes as $estudiante) {
        $clase->assignarAlumnos($estudiante);
    }

    $clase->darClase($profe, $estudiantes);
?>