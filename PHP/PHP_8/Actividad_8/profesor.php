<?php

    class Profesor extends Persona {

        protected $materia="matematicas";

        public function __construct($nombre, $edad, $materia, $sexo) {
            Persona::__construct($nombre, $edad, $sexo="indefinido");

            if($materia == "matematicas" || $materia == "filosofia" || $materia == "fisica" ){
                $this->materia = $materia;
            }

        }

        public function getMateria() {
            return $this->materia;
        }

        public function setMateria($materia) {
            $this->materia = $materia;
        }
    }

?>


