<?php
    class Operacion {
        protected $valor1;
        protected $valor2;
        protected $resultado;
        
        public function cargar1($v){
            $this->valor1 = $v;
        }
        
        public function cargar2($v){
            $this->valor2 = $v;
        }
        
        public function imprimirResultado(){
            echo $this->resultado.'<br>';
        }

    }

    class Suma extends Operacion{
        public function operar(){
            echo 'El resultado de la suma de '.$this -> valor1.' + '.$this->valor2.' es: ';
            $this->resultado = $this->valor1 + $this->valor2;
        }
        
        public function setNumero1($numero){
            $this->valor1 = $numero;
        }
    }
    
    $numero1 = $_REQUEST['valor1'];
    $numero2 = $_REQUEST['valor2'];

    $suma=new Suma();
    $operacion = new Operacion();
    
    $suma->cargar1($numero1);
    $suma->setNumero1(10);
    $suma->cargar2($numero2);
    $suma->operar();
    $suma->imprimirResultado();

?>