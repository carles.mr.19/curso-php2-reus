<?php

class Electrodomestico {

    protected $precio = 100;
    protected $color = "blanco";
    protected $consumo = "F";
    protected $peso = 5;
    
    protected $coloresDisponibles = array("Blanco", "Negro", "Rojo", "Azul", "Gris");
    protected $consumoEnergeticoDisponible = array("A", "B", "C", "D", "E", "F");

    public function __construct($precio, $peso) {
        $this->precio = $precio;
        $this->peso = $peso;
    }
    
    public function getPrecio() {
        return $this->precio;
    }

    public function getColor() {
        return $this->color;
    }

    public function getConsumo() {
        return $this->consumo;
    }

    public function getPeso() {
        return $this->peso;
    }

    public function comprobarConsumoEnergetico($letra) {
        for($i = 0; $i < count($this -> consumoEnergeticoDisponible); $i++){
            if($letra == $this -> consumoEnergeticoDisponible[$i]){
                $this -> consumoEnergetico = $letra;
            }
        }
    }

    public function comprobarColor($color) {
        for($i = 0; $i < count($this -> coloresDisponibles); $i++){
            if($color == $this -> coloresDisponibles[$i]){
                $this -> color = $color;
            }
        }
    }

    
    public function precioFinal(){
        switch ($this->consumo) {
            case "A": $this->precio +=100;
                break;
            case "B": $this->precio +=80;
                break;
            case "C": $this->precio +=60;
                break;
            case "D": $this->precio +=50;
                break;
            case "E": $this->precio +=30;
                break;
            case "F": $this->precio +=10;
                break;
        }
        
        if($this->peso>=0 && $this->peso<=19){
            $this->precio +=10;
        }else if($this->peso>=20 && $this->peso<=49){
            $this->precio +=50;
        }else if($this->peso>=50 && $this->peso<=79){
            $this->precio +=80;
        }else if($this->peso>=80){
            $this->precio +=100;
        }
    }
}

?>