<?php

require 'electrodomesticos.php';
require 'lavadora.php';
require 'television.php';

$totalprice = 0;
$totalLavadora = 0;
$totalTv = 0;

$electrodomesticos = [
    new Lavadora(150, 10, "verde", "c", 40),
    new Lavadora(100, 40, "amarillo", "a", 20),
    new Lavadora(150, 50, "rojo", "b", 30),
    new Lavadora(300, 30, "azul", "d", 10),
    new Lavadora(400, 24, "rojo", "f", 65),

    new Television(150, 10, "verde", "c", 41, false), 
    new Television(100, 30, "negro", "c", 24, true), 
    new Television(170, 20, "negro", "c", 24, true), 
    new Television(176, 25, "azul", "f", 30, false), 
    new Television(100, 45, "rojo", "c"), 
];


foreach ($electrodomesticos as $electrodomestico) {
    
    if($electrodomestico instanceof Lavadora){
        echo "Lavadora: ";
        $electrodomestico->precioFinal();
        $totalLavadora+= $electrodomestico->getPrecio();        
    }
    
    if($electrodomestico instanceof Television){    
        echo "Television: ";
        $electrodomestico->precioFinal();
        $totalTv+= $electrodomestico->getPrecio();        
    }
     echo "<br>"; 
}

echo "<br>El total de las lavadoras es: $totalLavadora";
echo "<br>El total de los televisores es: $totalTv";
echo "<br>El total de los articulos es: ".($totalLavadora + $totalTv);

