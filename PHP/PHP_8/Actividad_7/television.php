<?php
    class Television extends Electrodomestico {

    private $resolucion;
    private $sintonizador;

    public function __construct($precio, $peso, $color, $consumo, $resolucion=20, $sintonizador=false) {
        parent::__construct($precio, $peso);       
        $this->peso = $peso;
        $this->comprobarColor($color);
        $this->comprobarConsumoEnergetico($consumo);
        $this->resolucion = $resolucion;
        $this->sintonizador = $sintonizador;
    }
    
    public function getResolucion() {
        return $this->resolucion;
    }

    public function getSintonizador() {
        return $this->sintonizador;
    }

        
    public function precioFinal() {
        parent::precioFinal();
        
        if($this->getResolucion() > 40){
            $this->precio *=1.3;
        }
        
        if($this->getSintonizador()){
            $this->precio +=50;
        }
        
        echo "Precio final = ".$this->getPrecio();
    }
}
?>
