<?php
    include '../conexion.php';
    $conexion = abrirConexion();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $nombreFabricante = $_REQUEST["nombreFabricante"];
            $nombreArticulo = $_REQUEST["nombreArticulo"];
            
            if(isset($nombreFabricante)){
                echo "<h1> Fabricante </h1>";
                $idFabricante = conseguirIdFabricante($conexion, $nombreFabricante);
                echo buscarFabricante($conexion, $idFabricante);
            }
            
            echo "<br>";
            
            if(isset($nombreArticulo) && $nombreArticulo != ""){
                echo "<h1> Articulo </h1>";
                $idArticulo = conseguirIdArticulo($conexion, $nombreArticulo);
                echo buscarArticulo($conexion, $idArticulo);
            }
            
        ?>
        
        <?php
            cerrarConexion($conexion);
        ?>
    </body>
</html>
