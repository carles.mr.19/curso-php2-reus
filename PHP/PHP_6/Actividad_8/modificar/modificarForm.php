<?php
    include '../conexion.php';
    $conexion = abrirConexion();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <form action="modificarFinal.php" method="post">
        <?php
        
        $nombreFabricante = $_REQUEST["selectorFabricantes"];
        
        if(isset($nombreFabricante) && $nombreFabricante != ""){
        ?> 
        <h1>Fabricante</h1>    
            <input type="text" name ="nuevoNombreFabricante" value="<?php echo $nombreFabricante?>">
            <input type="hidden" name ="viejoNombreFabricante" value="<?php echo $nombreFabricante?>">
            <input type="submit" value="Modificar">
        <?php
        }
        ?>
            
        <?php
        
        $nombreArticulo = $_REQUEST["selectorArticulos"];
        $fabricanteArticulo = getFabricanteArticulo($conexion, $nombreArticulo);
        $precioArticulo = getPrecioArticulo($conexion, $nombreArticulo);
        
        if(isset($nombreArticulo) && $nombreArticulo != ""){
        ?> 
        <h1>Articulo</h1>    
            <input type="text" name="nuevoNombreArticulo" value="<?php echo $nombreArticulo?>">
            <input type="hidden" name="viejoNombreArticulo" value="<?php echo $nombreArticulo?>">
            <input type="text" name ="nuevoPrecioArticulo" value="<?php echo $precioArticulo?>">
            <input type="hidden" name ="viejoPrecioArticulo" value="<?php echo $precioArticulo?>">
            <input type="text" name ="nuevoFabricanteArticulo" value="<?php echo $fabricanteArticulo?>">
            <input type="hidden" name ="viejoFabricanteArticulo" value="<?php echo $fabricanteArticulo?>">
            <input type="submit" value="Modificar">
        
        <?php
        }
        ?>
        
        </form>
               
        <?php
            cerrarConexion($conexion); 
        ?>
    </body>
</html>
