<?php
    include '../conexion.php';
    $conexion = abrirConexion();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Fabricantes</h1>
        <p>Que fabricante quieres modificar?</p><br>
        <form method="post" action="modificarForm.php">
            <select name="selectorFabricantes">
                <option value=""></option>
                <?php
                
                $selectFabricantes = mysqli_query($conexion, "SELECT nombre FROM fabricantes") or die ("Problemas al seleccionar");
                
                while($registroFabricantes = mysqli_fetch_array($selectFabricantes)){
                    echo "<option value='$registroFabricantes[0]'>$registroFabricantes[0]</option>";
                }
                ?>
            </select>
            <input type="submit" value="Modificar">
            
            <br><p>Que fabricante quieres modificar?</p><br>
                  
            <select name="selectorArticulos">
                <option value=""></option>
                <?php
                    echo "<h1>Articulos</h1>";    

                    $selectArticulos = mysqli_query($conexion, "SELECT nombre FROM articulos") or die ("Problemas al seleccionar");

                    while($registroArticulos = mysqli_fetch_array($selectArticulos)){
                         echo "<option value='$registroArticulos[0]'>$registroArticulos[0]</option>";
                    }

                ?>
            </select>  
            
            <input type="submit" value="Modificar">
            
        </form>    
        
        <?php
            cerrarConexion($conexion);
        ?>
    </body>
</html>
