<?php
    include '../conexion.php';
    $conexion = abrirConexion();
    
    if(isset($_REQUEST["viejoNombreFabricante"])){
        $viejoNombreFabricante = $_REQUEST["viejoNombreFabricante"];
        $nuevoNombreFabricante = $_REQUEST["nuevoNombreFabricante"];
    }
    
    
    
    $viejoNombreArticulo = $_REQUEST["viejoNombreArticulo"];
    $nuevoNombreArticulo = $_REQUEST["nuevoNombreArticulo"];
    
    $viejoPrecioArticulo = $_REQUEST["viejoPrecioArticulo"];
    $nuevoPrecioArticulo = $_REQUEST["nuevoPrecioArticulo"];
    $viejoIdFabricanteArticulo = $_REQUEST["viejoFabricanteArticulo"];
    $nuevoIdFabricanteArticulo= $_REQUEST["nuevoFabricanteArticulo"];
    
    if(isset($viejoNombreFabricante) && isset($nuevoNombreFabricante)){
        $idFabricante = conseguirIdFabricante($conexion, $viejoNombreFabricante);
        if(modificarFabricante($conexion, $nuevoNombreFabricante, $idFabricante)){
            echo "Se ha modificado el Fabricante correctamente.<br>";
        }
    }
    
    if(isset($viejoNombreArticulo) && isset($nuevoNombreArticulo) && isset($viejoPrecioArticulo) && isset($nuevoPrecioArticulo) && isset($viejoIdFabricanteArticulo) && isset($nuevoIdFabricanteArticulo)){
        $idArticulo = conseguirIdArticulo($conexion, $viejoNombreArticulo);
        if(modificarArticulo($conexion, $nuevoNombreArticulo, $nuevoPrecioArticulo, $nuevoIdFabricanteArticulo, $idArticulo)){
            echo "Se ha modificado el Articulo correctamente.<br>";
        }
    }
    
    cerrarConexion($conexion);
?>

