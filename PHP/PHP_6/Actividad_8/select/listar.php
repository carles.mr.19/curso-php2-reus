<?php
    include '../conexion.php';
    $conexion = abrirConexion();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Fabricantes</h1><br>
            <?php
                $registrosFabricantes = mostrarFabricantes($conexion);

                foreach($registrosFabricantes as $values){
                    echo $values."<br>";      
                }
            ?>
        <h1>Articulos</h1><br>
            <?php
                $registrosArticulos = mostrarArticulos($conexion);
            
                foreach($registrosArticulos as $values){
                    echo $values."<br>";      
                }
            
                cerrarConexion($conexion);
            ?>
    </body>
</html>
