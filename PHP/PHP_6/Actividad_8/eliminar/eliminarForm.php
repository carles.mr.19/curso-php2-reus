<?php
    include '../conexion.php';
    $conexion = abrirConexion();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $fabricante = $_REQUEST["selectorFabricantes"];
            $articulo = $_REQUEST["selectorArticulos"];
            
            if(isset($fabricante) && $fabricante != ""){
                $idFabricante = conseguirIdFabricante($conexion, $fabricante);
                eliminarFabricante($conexion, $idFabricante);
                
                echo "El Fabricante $fabricante se ha eliminado correctamente<br>";
            }
            
            if(isset($articulo) && $articulo != ""){
                $idArticulo = conseguirIdFabricante($conexion, $articulo);
                eliminarFabricante($conexion, $idArticulo);
            
                echo "El Articulo $articulo se ha eliminado correctamente<br>";
            }
        ?>
        
        
        <?php
            cerrarConexion($conexion);
        ?>
    </body>
</html>
