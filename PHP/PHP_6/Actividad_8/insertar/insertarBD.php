<?php
    include '../conexion.php';
    $conexion = abrirConexion();
    
    //Recogida de datos
    //Fabricantes
    $nombreFabricante = $_REQUEST["nombreFabricante"];
    
    //Articulos
    $nombreArticulo = $_REQUEST["nombreArticulo"];
    $precioArticulo = $_REQUEST["precioArticulo"];
    $fabricanteArticulo = $_REQUEST["selector"];
    
   
    
    //Si se recibimos un Nombre de Fabricante.
    if(isset($nombreFabricante) && $nombreFabricante != ""){
        insertarFabricantes($conexion, $nombreFabricante);
        echo "Se ha introcuido el Fabricante con exito<br>";
    }
    
    //Si recibimos el nombre, precio del Articulo y el nombre del Fabricante.
    if((isset($nombreArticulo) && $nombreArticulo != "") && (isset($precioArticulo) && $precioArticulo != "") && (isset($fabricanteArticulo) && $fabricanteArticulo != "")){
        $idFabricante = conseguirIdFabricante($conexion, $fabricanteArticulo);
        insertarArticulo($conexion, $nombreArticulo, $precioArticulo, $idFabricante);
        echo "Se ha introcuido el Articulo con exito";        
    }
    
    cerrarConexion($conexion);
?>