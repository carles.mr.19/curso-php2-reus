<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php            
            $cadenaNombre = $_REQUEST["nombre"]." ".$_REQUEST["direccion"];
            $puntos = "\n................................................\n";
            
            $myFile = fopen("text/pedidos.txt", "w") or die ("Imposible abrir el fichero");
            fwrite($myFile, $cadenaNombre);
            fwrite($myFile, $puntos);
            
            if(isset($_REQUEST["cbJamon"]) && $_REQUEST["cJamon"] != ""){
                fwrite($myFile, "Pizza de Jamon ".$_REQUEST["cJamon"]);
                fwrite($myFile, $puntos);
            }
            
            if(isset($_REQUEST["cbNapo"])  && $_REQUEST["cNapo"] != ""){
                fwrite($myFile, "Pizza Napolitana ".$_REQUEST["cNapo"]);
                fwrite($myFile, $puntos);
            }
            
            if(isset($_REQUEST["cbMuzza"])  && $_REQUEST["cMuzza"] != ""){
                fwrite($myFile, "Pizza Muzarella ".$_REQUEST["cMuzza"]);
                fwrite($myFile, $puntos);
            }
            
            if(fclose($myFile)){
                echo "Pedido Guardado";
            }
        ?>
    </body>
</html>
