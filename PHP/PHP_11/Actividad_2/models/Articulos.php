<?php

class Articulos {
    private $db;
    private $id;
    public $descripcion;
    public $precio;
    public $codigoRubro;
    
    public function __construct() {
        $this -> db = Conectar::conexion();
    }

    function getId() {
        return $this->id;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getPrecio() {
        return $this->precio;
    }

    function getCodigoRubro() {
        return $this->codigoRubro;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function setCodigoRubro($codigoRubro) {
        $this->codigoRubro = $codigoRubro;
    }
      
    public function guardarArticulo(){
        $consulta = $this -> db -> query("INSERT INTO articulos (descripcion, precio, codigoRubro) VALUES ('{$this->getDescripcion()}','{$this->getPrecio()}','{$this->getCodigoRubro()}')") or die(mysql_error());
        
        if($consulta){
            header("Location:views/comprobaciones/guardadoOk.php");
        }else{
            header("Location:views/comprobaciones/guardadoFail.php");
        }    
    }
}

