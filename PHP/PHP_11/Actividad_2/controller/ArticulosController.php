<?php
    require_once("models/Articulos.php");
    
    class ArticulosController{
    
        public function crear() {
            require_once('view/articulos_views.php');
        }
        
        public function guardar(){
            $articulo = new Articulos();
            $rubro = new Rubro();
            
            $nombre = $_REQUEST['nombre'];
            $precio = $_REQUEST['precio'];
            $codigoRubro = $_REQUEST['selectorRubro'];
            
            $articulo ->setDescripcion($nombre);
            $articulo -> setPrecio($precio);
            $articulo -> setCodigoRubro($codigoRubro);
            
            $articulo -> guardarArticulo();
            
          }
    }

    

?>