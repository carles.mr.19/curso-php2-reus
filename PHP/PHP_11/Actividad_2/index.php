<?php
    require_once("db/db.php");
    
    require_once("controller/ArticulosController.php");
    require_once("controller/RubroController.php");
    $rubro = new Rubro();
    $rubros = $rubro -> getAll();
    require_once("views/articulos_view.php");
    
    if(isset($_GET['action']) == "crear"){
        $controladorArticulos = new ArticulosController();
        $controladorArticulos ->guardar();
    }
        
?>
