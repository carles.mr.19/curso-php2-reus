<?php

require_once 'models/Articulo.php';

class ArticuloController {

    public function index() {
        header("Location: index.php?controller=articulo&action=tabla");
    }

    public function tabla() {
        $articulo = new Articulo();
        $articulos = $articulo->getAllAndRubro();
        
        require_once 'views/articulos/tabla.php';
    }

    public function aumentar() {     
        $articulos = new Articulo();
        $articulos->aumentar();  
    }    
}
