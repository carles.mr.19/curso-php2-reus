<?php

class Articulo {
    
    public $db;
    public $codigo;
    public $descripcion;
    public $precio;
    public $codigorubro;

    public function __construct() {
        $this->db = database::conectar();
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getPrecio() {
        return $this->precio;
    }

    public function getCodigorubro() {
        return $this->codigorubro;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setPrecio($precio) {
        $this->precio = $precio;
    }

    public function setCodigorubro($codigorubro) {
        $this->codigorubro = $codigorubro;
    }

    public function getAllAndRubro() {
        $sql = "SELECT a.*, r.descripcion AS rubros FROM articulos a INNER JOIN rubros r ON a.codigorubro = r.codigo";
        $guardado = $this->db->query($sql);
        return $guardado;
    }

    public function aumentar() {
        $sql = "UPDATE articulos SET precio = precio * 1.10 WHERE precio <= 5";

        $guardado = $this->db->query($sql);
        
        printf("Affected rows (SELECT): %d\n", mysqli_affected_rows($this->db));
    }

   

}
