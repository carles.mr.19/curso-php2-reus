<?php

class Articulos {
    private $db;
    private $id;
    public $descripcion;
    public $precio;
    public $codigoRubro;
    
    public function __construct() {
        $this -> db = Conectar::conexion();
    }

    function getId() {
        return $this->id;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getPrecio() {
        return $this->precio;
    }

    function getCodigoRubro() {
        return $this->codigoRubro;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function setCodigoRubro($codigoRubro) {
        $this->codigoRubro = $codigoRubro;
    }
      
    public function getAllAndRubro() {
        $sql = "SELECT a.*, r.descripcion AS rubros FROM articulos a INNER JOIN rubros r ON a.codigorubro = r.codigo";
        $guardado = $this->db->query($sql);
        return $guardado;
    }

}

