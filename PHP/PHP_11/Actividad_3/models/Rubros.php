<?php

class Rubro {
    private $db;
    private $codigo;
    public $descripcion;
    
    public function __construct() {
        $this -> db = Conectar::conexion();
    }

    public function getCodigo(){
        return $this -> codigo;
    }
    
    public function setCodigo($codigo){
        $this -> codigo = $codigo;
    }
    
    public function getDescripcion(){
        return $this -> descripcion;
    }
    
    public function setDescripcion($descripcion){
        $this -> descripcion = $descripcion;
    }
    
    public function getAll() {
        $sql = "SELECT * FROM rubros";

        $rubros = $this->db->query($sql);

        return $rubros;
    }
    
    public function getByName($nombre){
        $sql = "SELECT * FROM rubros WHERE descripcion = '{$nombre}'";
        $select = $this->db->query($sql);
        
        if($registro = mysqli_fetch_array($select)){
            return $registro['codigo'];
        }
    }
}

