<?php
    require_once 'db/database.php';
    require_once 'controllers/ArticuloController.php';


    $controlador = new ArticuloController();
	
    if(isset($_GET['action']) && method_exists($controlador, $_GET['action'])){
        $action = $_GET['action'];
	$controlador->$action();
    }
        

