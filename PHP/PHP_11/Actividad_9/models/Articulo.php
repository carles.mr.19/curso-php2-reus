<?php

    class Articulo{

        public $db;
        public $codigo;
        public $descripcion;
        public $precio;
        public $codigorubro;

        public function __construct() {
            $this->db = database::conectar();
        }

        public function getCodigo() {
            return $this->codigo;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }

        public function getPrecio() {
            return $this->precio;
        }

        public function getCodigorubro() {
            return $this->codigorubro;
        }

        public function setCodigo($codigo) {
            $this->codigo = $codigo;
        }

        public function setDescripcion($descripcion) {
            $this->descripcion = $descripcion;
        }

        public function setPrecio($precio) {
            $this->precio = $precio;
        }

        public function setCodigorubro($codigorubro) {
            $this->codigorubro = $codigorubro;
        }

        public function getAllAndRubro() {
            $sql = "SELECT a.*, r.descripcion AS rubros FROM articulos a INNER JOIN rubros r ON a.codigorubro = r.codigo";
            $guardado = $this->db->query($sql);
            return $guardado;
        }

        public function getOne() {
            $sql = "SELECT * FROM articulos WHERE codigo = '{$this->getCodigo()}'";

            $guardado = $this->db->query($sql);

            if ($registro = mysqli_fetch_array($guardado)) {
                return true;
            } else {
                return false;
            }
        }

        public function destroy() {
            $sql = "Delete FROM articulos WHERE codigo = '{$this->getCodigo()}'";

            $guardado = $this->db->query($sql);

            printf("Total de filas afectaddas: %d\n", mysqli_affected_rows($this->db));
        }

    }