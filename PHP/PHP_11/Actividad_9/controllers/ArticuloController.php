<?php

require_once 'models/Articulo.php';

class ArticuloController {

    public function index() {
        header("Location: index.php?controller=articulo&action=tabla");
    }

    public function tabla() {
        $articulo = new Articulo();
        $articulos = $articulo->getAllAndRubro();
        require_once 'views/articulos/tabla.php';
    }

    public function destroy() {
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $codigo = $_REQUEST['id'];
            $articulo = new Articulo();
            $articulo->setCodigo($codigo);

            if ($articulo->getOne()) {
                $articulo->destroy();
                
            } else {
                echo "No existe el ID $codigo";
            }
        }
    }

}
