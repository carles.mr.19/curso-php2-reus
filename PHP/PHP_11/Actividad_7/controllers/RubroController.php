<?php

// Modelo
require_once 'models/Rubro.php';

class RubroController {

    public function index() {
        header("Location: index.php?controller=Rubro&action=buscar");
    }

    public function crear() {
        //Vista
        require_once 'views/rubros/crear.php';
    }

    public function buscar() {
        //Vista
        require_once 'views/rubros/buscar.php';
    }

    public function devolver() {
        if (isset($_REQUEST['codigo']) && is_numeric($_REQUEST['codigo'])) {
            $rubro = new Rubro();
            $codigo = + $_REQUEST['codigo'];
            $rubro->setCodigo($codigo);

            //Almacenar el valor que de la base de datos
            $rubro = $rubro->getFindOne();


            //Vista
            require_once 'views/rubros/listarUno.php';
        }
    }

    public function guardar() {
        if (isset($_REQUEST['descripcion']) && !empty($_REQUEST)) {
            $desc = $_REQUEST['descripcion'];
            $rubro = new Rubro();
            $rubro->setDescripcion($desc);

            if (!$rubro->getOne()) {
                $rubro->guardar();
            }
        }

        header("Location: index.php?controller=Rubro&action=crear");
    }

}
