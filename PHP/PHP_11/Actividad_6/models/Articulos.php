<?php

class Articulos {
    private $db;
    private $id;
    public $descripcion;
    public $precio;
    public $codigoRubro;
    
    public function __construct() {
        $this -> db = Conectar::conexion();
    }

    function getId() {
        return $this->id;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getPrecio() {
        return $this->precio;
    }

    function getCodigoRubro() {
        return $this->codigoRubro;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function setCodigoRubro($codigoRubro) {
        $this->codigoRubro = $codigoRubro;
    }
      
    public function eliminar() {
        $sql = "DELETE FROM articulos WHERE id = '{$this->id}'";
        $eliminar = $this->db->query($sql) or die(mysql_error($this->db));
        
        if($eliminar){
            echo "Se ha eliminado correctamente";
        }
        
    }

}

