<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $a = 1;
            $b = 2;
            $c = 3;
            $d = 4;
            
            echo "Valor A = ".$a."<br>";
            echo "Valor B = ".$b."<br>";
            echo "Valor C = ".$c."<br>";
            echo "Valor D = ".$d."<br>";
            
            $b = $c;
            echo "<br>"."Valor de B a C = ".$b."<br>";
            
            $c = $a;
            echo "Valor de C a A = ".$c."<br>";
            
            $a = $d;
            echo "Valor de A a D = ".$a."<br>";
            
            $d = $b;
            echo "Valor de D a B = ".$d."<br>";
        ?>
    </body>
</html>
