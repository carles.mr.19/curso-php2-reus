<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $x = 10;
            $y = 9;
            
            $n = 1.5;
            $m = 2.5;
            
            echo "Variable X = ". $x."<br>";
            echo "Variable Y = ". $y."<br>";
            echo "Variable N = ".$n."<br>";
            echo "Variable M = ".$m."<br>";
            
            $suma = $n + $m;
            echo "<br>"."Suma de X + Y = ".$suma."<br>";
            
            $resta = $n - $m;
            echo "Diferencia X - Y = ".$resta."<br>";
            
            $producto = $x * $y;
            echo "Producto X * Y = ".$producto."<br>";
            
            $cociente = $x / $y;
            echo "Cociente X / Y = ".$cociente."<br>";
            
            $resto = $x % $y;
            echo "Resto X % Y = ".$resto."<br>";
            
            $suma2 = $n + $m;
            echo "Suma N + M = ".$suma2."<br>";
            
            $resta2 = $n - $m;
            echo "Diferencia N - M = ".$resta2."<br>";
            
            $producto2 = $n * $m;
            echo "Producto N * M = ".$producto2."<br>";
            
            $cociente2 = $n / $m;
            echo "Cociente N / M = ".$cociente2."<br>";
            
            $resto2 = $n % $m;
            echo "Cociente N % M = ".$resto2."<br>";
            
            $suma3 = $x + $n;
            echo "Cociente X + N = ".$suma3."<br>";
            
            $cociente3 = $y / $m;
            echo "Cociente Y / M = ".$cociente3."<br>";
            
            $resto3 = $y % $m;
            echo "Cociente Y % M = ".$resto3."<br>";
            
            echo "<br>"."Doble de cada variable"."<br>";
            echo "Variable X = ". $x * 2;
            echo "<br>";
            echo "Variable Y = ". $y * 2;
            echo "<br>";
            echo "Variable N = ". $n * 2;
            echo "<br>";
            echo "Variable M = ". $m * 2;
            echo "<br>";
            
            echo "<br>"."Suma de todas las variables"."<br>";
            $suma4 = $x + $y + $m + $n;
            echo $suma4."<br>";
            
            echo "<br>"."Producto de todas las variables"."<br>";
            $producto3 = $x * $y * $m * $n;
            echo $producto3;
        ?>
    </body>
</html>
