<?php
session_start();

$mayusculas = $_REQUEST["mayusculas"];
$minusculas = $_REQUEST["minusculas"];


if ($mayusculas == "") {
    $_SESSION["mayusculasError"] = "No ha escrito ninguna palabra";
    
}elseif(!ctype_upper($mayusculas)) {
    $_SESSION["mayusculasError"] = "No ha escrito la palabra en mayúsculas";
    
}else{
    $_SESSION["mayusculas"] = $mayusculas;
}

if ($minusculas == "") {
    $_SESSION["minusculasError"] = "No ha escrito ninguna palabra";
    
} elseif (!ctype_lower($minusculas)) {
    $_SESSION["minusculasError"] = "No ha escrito la palabra en minúsculas";
    
} else {
    $_SESSION["minusculas"] = $minusculas;
}


// Volvemos al formulario 8
header("Location:index.php");
?>