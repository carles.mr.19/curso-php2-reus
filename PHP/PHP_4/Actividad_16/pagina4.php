<?php
    session_start();
    
    $palabra2 = $_REQUEST["palabra2"];
    $_SESSION["palabra2"] = $palabra2;
    
    $numeroPalabras = str_word_count($palabra2, 0);
    $palabra1 = $_SESSION["palabra1"];
    
    if ($palabra2 == "") {
        $_SESSION["error"] = "No has escrito ninguna palabra.";
        header("Location:pagina3.php");
    }elseif($numeroPalabras > 1){
        $_SESSION["error"] = "Has escrito mas de una palabra.";
        header("Location:pagina3.php");
    }elseif(comprobarCaracteres($palabra2) == 0){
        $_SESSION["error"] = "Has escrito un caracter no permitido.";
        header("Location:pagina3.php");
    }else{
        header("Location:pagina5.php");
    }
    
    function comprobarCaracteres($palabra){
        $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i<strlen($palabra); $i++){
            if (strpos($permitidos, substr($palabra,$i,1))===false){
                return false;
            }
        }
        return true;
    }
    
?>