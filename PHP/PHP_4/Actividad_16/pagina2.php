<?php
    session_start();
    
    $palabra = $_REQUEST["palabra1"];
    $_SESSION["palabra1"] = $palabra;
    
    echo $numeroPalabras = str_word_count($palabra, 0);

    if ($palabra == "") {
        $_SESSION["error"] = "No has escrito ninguna palabra.";
        header("Location:index.php");
    }elseif($numeroPalabras > 1){
        $_SESSION["error"] = "Has escrito mas de una palabra.";
        header("Location:index.php");
    }elseif(comprobarCaracteres($palabra) == 0){
        $_SESSION["error"] = "Has escrito un caracter no permitido.";
        header("Location:index.php");
    }else{
        header("Location:pagina3.php");
    }
    
    function comprobarCaracteres($palabra){
        $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i<strlen($palabra); $i++){
            if (strpos($permitidos, substr($palabra,$i,1))===false){
                return false;
            }
        }
        return true;
    }
    
?>