<?php
    session_start();
    
    if(isset($_REQUEST['pais'])){
        $paisSeleccionado = $_REQUEST['pais'];
    }else{
        $paisSeleccionado = "francia";
    }
    
    $pais = array("Francia", "España");
    $ciudades["francia"][0] = "Paris";
    $ciudades["francia"][1] = "Orleans";
    $ciudades["españa"][0] = "Madrid";
    $ciudades["españa"][1] = "Barcelona";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <form action="index.php" action="post"> 
        Pais:<br>  
            <select name="pais">
                <option value="francia">Francia</option>
                <option value="españa">España</option>
            </select> 
        <?php
            $_SESSION["paisSeleccionado"] = $paisSeleccionado;
            
            foreach($ciudades as $ciudad => $value){
                if($ciudad == $paisSeleccionado){
                    foreach($value as $valueSelect){
                        echo $valueSelect."<br>";
                    }
                }
            }
        ?>
        
        <input type="submit" value="Comprobar">
        </form>
    </body>
</html>
