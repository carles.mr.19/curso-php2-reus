<?php
    session_start();
    
    $palabra = $_REQUEST["palabra"];
    $_SESSION["palabra"] = $palabra;

    if ($palabra == "") {
        $_SESSION["error"] = "No ha escrito ninguna palabra";
    }elseif(!ctype_upper($palabra)) {
        $_SESSION["error"] = "No ha escrito la palabra en mayúsculas";
    }

    header("Location:index.php");
?>