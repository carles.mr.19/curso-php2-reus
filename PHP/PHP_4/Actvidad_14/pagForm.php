<?php
session_start();

if (!isset($_SESSION["a"]) || !isset($_SESSION["b"])) {
    $_SESSION["a"] =  $_SESSION["b"] = 0;
}

$accion = $_REQUEST["accion"];

if ($accion == "a") {
    $_SESSION["a"] += 10;
} elseif ($accion == "b") {
    $_SESSION["b"] += 10;
} elseif ($accion == "cero") {
    $_SESSION["a"] = $_SESSION["b"] = 0;
}

header("Location:index.php");
?>