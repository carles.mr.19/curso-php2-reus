<?php
session_start();

if (!isset($_SESSION["numero"])) {
    $_SESSION["numero"] = 0;
}

$accion = $_REQUEST["accion"];

if ($accion == "cero") {
    $_SESSION["numero"] = 0;
} elseif ($accion == "subir") {
    $_SESSION["numero"] ++;
} elseif ($accion == "bajar") {
    $_SESSION["numero"] --;
}

header("Location:index.php");
?>