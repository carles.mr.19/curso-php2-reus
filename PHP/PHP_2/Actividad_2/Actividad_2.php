<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            echo "Tabla de Multiplicar del 2 (FOR)"."<br>";
            for($i = 0; $i < 12; $i = $i + 2){
                echo $i ." ";
            }
            
            echo "<br>"."<br>";
            echo "Tabla de Multiplicar del 2 (WHILE)"."<br>";
            $cont = 0;
            while($cont != 6){
                echo $cont * 2 ." ";
                $cont++;
            }
            
            echo "<br>"."<br>";
            echo "Tabla de Multiplicar del 2 (DO WHILE)"."<br>";
            $cont = 0;
            do{
                echo $cont * 2 ." ";
                $cont++;
            }while(($cont != 6))
        ?>
    </body>
</html>
