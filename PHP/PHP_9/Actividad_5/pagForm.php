<?php
    abstract class Operacion {
        protected $valor1;
        protected $valor2;
        protected $resultado;
        
        public function cargar1($n1){
            $this->valor1=$n1;
        }
        
        public function cargar2($v){
            $this->valor2=$v;
        }
        
        public function imprimirResultado(){
            echo $this->resultado.'<br>';
        }
  
        public abstract function operar();
    }

    class Suma extends Operacion{
        public function operar(){
            $this->resultado=$this->valor1+$this->valor2;
        }
    }

    class Resta extends Operacion{
        public function operar(){
            $this->resultado=$this->valor1-$this->valor2;
        }
    }
    
    $numero1 = $_REQUEST['valor1'];
    $numero2 = $_REQUEST['valor2'];

    $suma = new Suma();
    $suma -> cargar1($numero1);
    $suma -> cargar2($numero2);
    $suma -> operar();
    echo 'El resultado de la suma es: ';
    $suma -> imprimirResultado();
    
    $resta = new Resta();
    $resta -> cargar1($numero1);
    $resta -> cargar2($numero2);
    $resta -> operar();
    echo 'El resultado de la resta es: ';
    $resta->imprimirResultado();
?>