<?php
    class Persona{
        private $nombre;
        private $edad;
        
        public function fijarNombreEdad($nombre, $edad) {
            $this -> nombre = $nombre;
            $this -> edad = $edad;
        }
        
        public function getNombre(){
            return $this -> nombre;
        }
        
        public function getEdad() {
            return $this -> edad;
        }
        
        public function __clone(){
            $this -> edad = 0;
        
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];
    
    $persona = new Persona();
    $persona ->fijarNombreEdad($nombre, $edad);
    
    echo 'Datos de $persona<br>';
    echo $persona ->getNombre().' '.$persona-> getEdad().'<br>';
    $persona2 = clone($persona);
    
    echo 'Datos de $persona2<br>';
    echo $persona2 -> getNombre().' '.$persona2 -> getEdad().'<br>';
?>