<?php
    class Operacion {
        protected $valor1 = 0;
        protected $valor2 = 0;
        
        public function __construct($n1, $n2) {
            $this->valor1 = $n1;
            $this->valor2 = $n2;
        }
    }
    
    class Suma extends Operacion{
        protected $titulo;
        
        public function __construct($n1, $n2, $titulo) {
            parent::__construct($n1, $n2);
            $this->titulo = $titulo;
        }
        
        public function resultado() {
            $suma = $this->valor1 + $this->valor2;
            echo $this->titulo." ".$suma; 
        }
    }
    
    $n1 = $_REQUEST['valor1'];
    $n2 = $_REQUEST['valor2'];
    
    $suma = new Suma($n1, $n2, "El resultado es: ");
    $suma ->resultado();
?>