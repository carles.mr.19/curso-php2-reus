<?php

    class Cuadrado {
        private $lado;
        
        public function cargarLado($la){
            $this->lado=$la;
        }
        
        public function retornarPerimetro(){
            $permietro = $this-> lado * 4;
            return $permietro;
        }
        
        public function retornarSuperficie(){
            $superficie = $this->lado * $this->lado;
            return $superficie;
        }
    }

    $lado = $_REQUEST['lado'];

    $cuadrado1 = new Cuadrado();
    $cuadrado1 -> cargarLado($lado);
    $cuadrado2 = $cuadrado1;
    
    echo 'La superficie del cuadrado de lado '.$lado.' es: '.$cuadrado2->retornarSuperficie().'<br>';
    echo 'El perímetro del cuadrado de lado '.$lado.' es: '.$cuadrado2->retornarPerimetro().'<br>';

?>