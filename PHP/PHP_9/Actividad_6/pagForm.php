<?php
    abstract class Trabajador {
        protected $nombre;
        protected $sueldo;
        
        public function __construct($nom){
            $this->nombre=$nom;
        }
        
        public abstract function calcularSueldo();
        
        public function imprimirDatos(){
            echo 'Nombre: '.$this -> nombre;
            echo ' Sueldo: '.$this -> sueldo.'<br>';
        }
        
    }

    class Empleado extends Trabajador{
        protected $horasTrabajadas;
        protected $valorHora;
        
        public function __construct($nom, $horasTrabajadas, $valorHora) {
            parent::__construct($nom);
            $this -> horasTrabajadas = $horasTrabajadas;
            $this -> valorHora = $valorHora;
        }
        
        public function calcularSueldo() {
            $this -> sueldo = $this -> horasTrabajadas * $this -> valorHora;
        }
    }
    
    class Gerente extends Trabajador{
        protected $utilidades;
        
        public function __construct($nom, $utilidades) {
            parent::__construct($nom);
            $this -> utilidades = $utilidades;
        }
        
        public function calcularSueldo() {
            $this -> sueldo = $this -> utilidades * 0.10;;
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $horasTrabajadas = $_REQUEST['hTrabajadas'];
    $valorHoras = $_REQUEST['vHoras'];
     
    $nombreGerente = $_REQUEST['nombreGerente'];
    $utilidades = $_REQUEST['utilidades'];
    
    $empleado = new Empleado ($nombre, $horasTrabajadas, $valorHoras);
    $empleado ->calcularSueldo();
    echo 'Sueldo EMPLEADO: <br>';
    $empleado -> imprimirDatos();
    
    $gerente = new Gerente($nombreGerente, $utilidades);
    $gerente ->calcularSueldo();
    echo '<br>Sueldo GERENTE: <br>';
    $gerente ->imprimirDatos();
?>