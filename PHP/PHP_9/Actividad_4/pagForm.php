<?php
    abstract class Persona {
        protected $nombre;
        protected $edad;
        
        public function cargarDatosPersonales($nom,$ed){
            $this->nombre=$nom;
            $this->edad=$ed;
        }
        
        public function imprimirDatosPersonales(){
            echo 'Nombre:'.$this->nombre.'<br>';
            echo 'Edad:'.$this->edad.'<br>';
        }
    }

    class Empleado extends Persona{
        protected $sueldo;
        
        public function cargarSueldo($su){
            $this->sueldo=$su;
        }
        
        public function imprimirSueldo(){
            echo 'Sueldo:'.$this->sueldo.'<br>';
        }
    }

    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];
    $sueldo = $_REQUEST['sueldo'];

    //$persona= new Persona();
    //$persona -> cargarDatosPersonales($nombre, $edad);
    //echo 'Datos de PERSONA: <br>';
    //$persona -> imprimirDatosPersonales();

    $empleado = new Empleado();
    $empleado -> cargarDatosPersonales($nombre, $edad);
    $empleado -> cargarSueldo($sueldo);
    echo 'Datos de PERSONA y EMPLEADO: <br>';
    $empleado -> imprimirDatosPersonales();
    $empleado -> imprimirSueldo();
?>