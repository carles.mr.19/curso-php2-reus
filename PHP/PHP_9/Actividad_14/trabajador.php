<?php
    class Trabajador{
        
        protected $nombre;
        protected $sueldo;

        public function __construct($nombre, $sueldo) {
            $this->nombre = $nombre;
            $this->sueldo = $sueldo;
        }
        
        public function getNombre(){
            return $this -> nombre;
        }

        public function getSueldo() {
            return $this->sueldo;
        }
    }
?>

