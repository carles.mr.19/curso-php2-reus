<?php
    require 'trabajador.php';
    require 'empleado.php';
    require 'gerente.php';

    $nombre = "";
    $maxCobra = 0;
    
    $nombre1 = $_REQUEST['nombre1'];
    $sueldo1 = $_REQUEST['sueldo1'];
    
    $nombre2 = $_REQUEST['nombre2'];
    $sueldo2 = $_REQUEST['sueldo2'];
    
    $nombre3 = $_REQUEST['nombre3'];
    $sueldo3 = $_REQUEST['sueldo3'];
    
    $nombre4 = $_REQUEST['nombre4'];
    $sueldo4 = $_REQUEST['sueldo4'];
    
    $nombre5 = $_REQUEST['nombre5'];
    $sueldo5 = $_REQUEST['sueldo5'];

    $trabajadores = [
        new Empleado($nombre1, $sueldo1),
        new Empleado($nombre2, $sueldo2),
        new Empleado($nombre3, $sueldo3),
    
        new Gerente($nombre4, $sueldo4),
        new Gerente($nombre5, $sueldo5),
    ];

    foreach ($trabajadores as $trabajador) {
        if($trabajador instanceof Gerente){
            if($trabajador->getSueldo() > $maxCobra){
                $nombre = $trabajador->getNombre();
                $maxCobra = $trabajador->getSueldo();
            }
        }
    }

    echo "<br>El GERENTE $nombre que mas gana $maxCobra €";

    
?>