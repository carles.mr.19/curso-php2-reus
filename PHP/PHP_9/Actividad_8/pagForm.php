<?php
    class Persona {
        protected $nombre;
        protected $edad;
        
        public final function cargarDatosPersonales($nom,$edad){
            $this -> nombre = $nom;
            $this -> edad = $edad;
        }
        
        public function imprimirDatosPersonales(){
            echo 'Nombre: '.$this->nombre.'<br>';
            echo 'Edad: '.$this->edad.'<br>';
        }
    }

    final class Empleado extends Persona{
        protected $sueldo;
        
        public function cargarSueldo($su){
            $this -> sueldo = $su;
        }
        
        public function imprimirSueldo(){
            echo 'Sueldo: '.$this -> sueldo.'<br>';
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];  
    
    $nombre2 = $_REQUEST['nombre2'];
    $edad2 = $_REQUEST['edad2'];
    $sueldo2 = $_REQUEST['sueldo2'];

    $persona1=new Persona();
    $persona1->cargarDatosPersonales($nombre, $edad);
    echo 'Datos de PERSONA:<br>';
    $persona1->imprimirDatosPersonales();
    $empleado1=New Empleado();
    $empleado1->cargarDatosPersonales($nombre2,$edad2);
    $empleado1->cargarSueldo($sueldo2);
    echo '<br>Datos de EMPLEADO:<br>';
    $empleado1->imprimirDatosPersonales();
    $empleado1->imprimirSueldo();
    
?>  