<?php

    class Persona {
        private $nombre;
        private $edad;
      
        public function fijarNombreEdad($nom,$ed) {
            $this->nombre = $nom;
            $this->edad = $ed;
        }
        
        public function retornarNombre() {
            return $this->nombre;
        }
        
        public function retornarEdad() {
            return $this->edad;
        }
        
        public function __clone(){
            $this->edad += 1;
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];

    $persona1=new Persona();
    $persona1->fijarNombreEdad($nombre, $edad);
    
    echo 'Persona 1: ';
    echo $persona1->retornarNombre().' - '.$persona1->retornarEdad().'<br>';
    
    $persona2=clone($persona1);
    
    echo 'Persona 2: ';
    echo $persona2->retornarNombre().' - '.$persona2->retornarEdad().'<br>';

?>