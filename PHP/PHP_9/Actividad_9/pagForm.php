<?php
    class Persona {
        protected $nombre;
        protected $edad;
        
        public function fijarNombreEdad($nom, $edad) {
            $this -> nombre = $nom;
            $this -> edad = $edad;
        }
        
        public function getNombre(){
            return $this -> nombre;
        }
        
        public function getEdad(){
            return $this -> edad;
        }
    }
    
    $nombre1 = $_REQUEST['nombre1'];
    $edad1 = $_REQUEST['edad1'];
    
    $nombre2 = $_REQUEST['nombre2'];
    $edad2 = $_REQUEST['edad2'];
    
    $nombre3 = $_REQUEST['nombre3'];
    $edad3 = $_REQUEST['edad3'];
    

    $persona1=new Persona();
    $persona1->fijarNombreEdad($nombre1, $edad1);
    $x=$persona1;
    echo 'Datos de la persona ($persona1):<br>';
    echo $persona1->getNombre().' - '.$persona1->getEdad().'<br><br>';
    echo 'Datos de la persona ($x):';
    echo $persona1->getNombre().' - '.$persona1->getEdad().'<br><br>';
    $x->fijarNombreEdad($nombre2, $edad2);
    echo 'Después de modificar los datos<br>';
    echo 'Datos de la persona ($persona1):<br>';
    echo $persona1->getNombre().' - '.$persona1->getEdad().'<br><br>';
    echo 'Datos de la persona ($x):<br>';
    echo $persona1->getNombre().' - '.$persona1->getEdad().'<br><br>';
    $persona2=clone($persona1);
    $persona1->fijarNombreEdad($nombre3, $edad3);
    echo 'Después de modificar los datos de persona1<br>';
    echo 'Datos de la persona ($persona1):<br>';
    echo $persona1->getNombre().' - '.$persona1->getEdad().'<br><br>';
    echo 'Datos de la persona ($persona2):<br>';
    echo $persona2->getNombre().' - '.$persona2->getEdad().'<br><br>';

    
   
?>