<?php
    class Persona {
        protected $nombre;
        protected $edad;
        
        public function __construct($nom,$ed){
            $this->nombre=$nom;
            $this->edad=$ed;
        }
        
        public function imprimirDatosPersonales(){
            echo 'Datos personales de PERSONA: <br>';
            echo 'Nombre:'.$this->nombre.'<br>';
            echo 'Edad:'.$this->edad.'<br>';
        }
    }

    class Empleado extends Persona{
        protected $sueldo;
        
        public function __construct($nom, $ed, $sueldo) {
            parent::__construct($nom, $ed);
            $this -> sueldo = $sueldo;
        }
        
        public function imprimirSueldo(){
            echo 'Sueldo:'.$this->sueldo.'<br>';
        }
    }
    
    $nombre = $_REQUEST['nombre'];
    $edad = $_REQUEST['edad'];
    $sueldo = $_REQUEST['sueldo'];
    
    $persona = new Persona($nombre, $edad);
    $persona ->imprimirDatosPersonales();
    
    $empleado1=New Empleado($nombre, $edad, $sueldo);
    $empleado1->imprimirSueldo();
?>