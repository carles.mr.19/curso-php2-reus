<?php
    class Operacion {
        protected $valor1;
        protected $valor2;
        protected $resultado;
        
        public function __construct($v1,$v2){
            $this->valor1=$v1;
            $this->valor2=$v2;
        }

        public final function imprimirResultado(){
            echo $this->resultado.'<br>';
        }
    }

    final class Suma extends Operacion{
        private $titulo;
        
        public function __construct($v1,$v2,$tit){
            parent::__construct($v1,$v2);
            $this->titulo=$tit;
        }
    
        public function operar(){
            echo $this->titulo.' ';
            echo $this->valor1.'+'.$this->valor2.' es ';
            $this->resultado=$this->valor1 + $this->valor2;
        }
    }

    $numero1 = $_REQUEST['valor1'];
    $numero2 = $_REQUEST['valor2'];
    $titulo = $_REQUEST['titulo'];
    
    $suma=new Suma($numero1, $numero2, $titulo);
    $suma->operar();
    $suma->imprimirResultado();

?>