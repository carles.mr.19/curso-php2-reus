<?php
    abstract class Operacion {
        protected $valor1;
        protected $valor2;
        protected $resultado;
        
        public function cargar1($n1){
            $this -> valor1 = $n1;
        }
        
        public function cargar2($n2){
            $this -> valor2 = $n2;
        }
        
        public function mostrarResultado(){
            echo 'El resultado es: ';
            echo $this -> resultado.'<br>';
        }
    }
    
    class Suma extends Operacion{
        public function operar(){
            $this -> resultado = $this -> valor1 + $this -> valor2;
        }
    }
    
    class Resta extends Operacion{
        public function operar(){
            $this -> resultado = $this -> valor1 - $this -> valor2;
        }
    }
    
    $n1 = $_REQUEST['valor1'];
    $n2 = $_REQUEST['valor2'];
    
    $suma = new Suma();
    $suma -> cargar1($n1);
    $suma ->cargar2($n2);
    $suma ->operar();
    echo "El resultado de la suma es: ";
    $suma ->mostrarResultado();
    
    $resta = new Resta();
    $resta -> cargar1($n1);
    $resta ->cargar2($n2);
    $resta ->operar();
    echo "El resultado de la resta es: ";
    $resta ->mostrarResultado();
?>